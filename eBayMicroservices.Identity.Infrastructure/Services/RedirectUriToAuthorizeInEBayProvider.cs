﻿using System.Web;
using eBayMicroservices.Identity.Application.Options;
using eBayMicroservices.Identity.Application.Services;

namespace eBayMicroservices.Identity.Infrastructure.Services
{
    public class RedirectUriToAuthorizeInEBayProvider : IRedirectUriToAuthorizeInEBayProvider
    {
        private readonly EBayDevelopConfig _eBayDevelopConfig;

        public RedirectUriToAuthorizeInEBayProvider(EBayDevelopConfig eBayDevelopConfig)
        {
            _eBayDevelopConfig = eBayDevelopConfig;
        }

        public string UrlToEBayAuthorize(bool sandbox)
        {
            string scopes = HttpUtility.UrlEncode("https://api.ebay.com/oauth/api_scope https://api.ebay.com/oauth/api_scope/buy.order.readonly https://api.ebay.com/oauth/api_scope/buy.guest.order https://api.ebay.com/oauth/api_scope/sell.marketing.readonly https://api.ebay.com/oauth/api_scope/sell.marketing https://api.ebay.com/oauth/api_scope/sell.inventory.readonly https://api.ebay.com/oauth/api_scope/sell.inventory https://api.ebay.com/oauth/api_scope/sell.account.readonly https://api.ebay.com/oauth/api_scope/sell.account https://api.ebay.com/oauth/api_scope/sell.fulfillment.readonly https://api.ebay.com/oauth/api_scope/sell.fulfillment https://api.ebay.com/oauth/api_scope/sell.analytics.readonly https://api.ebay.com/oauth/api_scope/sell.marketplace.insights.readonly https://api.ebay.com/oauth/api_scope/commerce.catalog.readonly https://api.ebay.com/oauth/api_scope/buy.shopping.cart https://api.ebay.com/oauth/api_scope/buy.offer.auction https://api.ebay.com/oauth/api_scope/commerce.identity.readonly https://api.ebay.com/oauth/api_scope/commerce.identity.email.readonly https://api.ebay.com/oauth/api_scope/commerce.identity.phone.readonly https://api.ebay.com/oauth/api_scope/commerce.identity.address.readonly https://api.ebay.com/oauth/api_scope/commerce.identity.name.readonly https://api.ebay.com/oauth/api_scope/commerce.identity.status.readonly https://api.ebay.com/oauth/api_scope/sell.finances https://api.ebay.com/oauth/api_scope/sell.item.draft https://api.ebay.com/oauth/api_scope/sell.payment.dispute https://api.ebay.com/oauth/api_scope/sell.item");

            string sandboxDomainAlias = sandbox? ".sandbox" : string.Empty;
            
            string clientId = sandbox ? _eBayDevelopConfig.Sandbox.AppId : _eBayDevelopConfig.Prod.AppId;
            
            string redirectUrl = sandbox ? _eBayDevelopConfig.Sandbox.RedirectUrl : _eBayDevelopConfig.Prod.RedirectUrl;
            
            string eBayLink = $"https://auth{sandboxDomainAlias}.ebay.com/oauth2/authorize?client_id={clientId}&response_type=code&redirect_uri={redirectUrl}&scope={scopes}";
            
            return eBayLink;
        }
    }
}