﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Infrastructure.Services.Models;
using Newtonsoft.Json;

namespace eBayMicroservices.Identity.Infrastructure.Services
{
    public class EBayIdentityServiceClient : IEBayIdentityServiceClient
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly string _url;

        public EBayIdentityServiceClient(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
            _url = "https://apiz.sandbox.ebay.com/commerce/identity/v1/user/";
        }

        public async Task<string> GetUserName(string userAccessToken)
        {
            using HttpClient client = _httpClientFactory.CreateClient();
            
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",userAccessToken);

            HttpResponseMessage result = await client.GetAsync(_url);

            if (!result.IsSuccessStatusCode)
            {
                return null;
            }

            string contentString = await result.Content.ReadAsStringAsync();
            
            GetUserNameResponseModel deserializedObject = JsonConvert.DeserializeObject<GetUserNameResponseModel>(contentString);

            return deserializedObject.Username;
        }
    }
}