﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Application.DTOs.Policies;
using eBayMicroservices.Identity.Application.Exceptions.Concrete;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Infrastructure.Services.Models;
using Newtonsoft.Json;

namespace eBayMicroservices.Identity.Infrastructure.Services
{
    public class EBayPoliciesServiceClient : IEBayPoliciesServiceClient
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public EBayPoliciesServiceClient(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        
        public async Task<IEnumerable<FulfillmentPolicyDto>> GetFulfillmentPolicies(string marketplace, string accessToken,bool isTest)
        {
            //string url = string.Format(_url,"fulfillment", marketplace);

            string url = PrepareUrl("fulfillment", isTest, marketplace);
            
            IList<FulfillmentPolicyDto> policies = new List<FulfillmentPolicyDto>();
            while (!string.IsNullOrWhiteSpace(url))
            {
                using HttpClient cyclicHttpClient = PrepareHttpClient(accessToken);
                HttpResponseMessage cyclicResult = await cyclicHttpClient.GetAsync(url);
                
                if (!cyclicResult.IsSuccessStatusCode)
                {
                    throw new EBayApiException((int)cyclicResult.StatusCode);
                }
                
                string cyclicContent = await cyclicResult.Content.ReadAsStringAsync();
                
                GetFulfillmentPoliciesResponseModel cyclicModel = JsonConvert.DeserializeObject<GetFulfillmentPoliciesResponseModel>(cyclicContent);
                
                foreach (FulfillmentPolicyModel policyModel in cyclicModel.FulfillmentPolicies)
                {
                    policies.Add(new FulfillmentPolicyDto
                    {
                        Id = policyModel.FulfillmentPolicyId,
                        Description = policyModel.Description,
                        Name = policyModel.Name
                    });
                }
                url = cyclicModel.Next;
            }

            return policies;
        }

        public async Task<IEnumerable<PaymentPolicyDto>> GetPaymentPolicies(string marketplace, string accessToken,bool isTest)
        {
            //string url = string.Format(_url,"payment", marketplace);
            string url = PrepareUrl("payment", isTest, marketplace);
            IList<PaymentPolicyDto> policies = new List<PaymentPolicyDto>();
            while (!string.IsNullOrWhiteSpace(url))
            {
                using HttpClient cyclicHttpClient = PrepareHttpClient(accessToken);
                HttpResponseMessage cyclicResult = await cyclicHttpClient.GetAsync(url);
                
                if (!cyclicResult.IsSuccessStatusCode)
                {
                    throw new EBayApiException((int)cyclicResult.StatusCode);
                }
                
                string cyclicContent = await cyclicResult.Content.ReadAsStringAsync();
                
                GetPaymentPoliciesResponseModel cyclicModel = JsonConvert.DeserializeObject<GetPaymentPoliciesResponseModel>(cyclicContent);
                
                foreach (PaymentPolicyModel policyModel in cyclicModel.PaymentPolicies)
                {
                    policies.Add(new PaymentPolicyDto
                    {
                        Id = policyModel.PaymentPolicyId,
                        Description = policyModel.Description,
                        Name = policyModel.Name
                    });
                }
                url = cyclicModel.Next;
            }

            return policies;
        }

        public async Task<IEnumerable<ReturnPolicyDto>> GetReturnPolicies(string marketplace, string accessToken,bool isTest)
        {
            //string url = string.Format(_url,"return", marketplace);
            string url = PrepareUrl("return", isTest, marketplace);
            IList<ReturnPolicyDto> policies = new List<ReturnPolicyDto>();
            while (!string.IsNullOrWhiteSpace(url))
            {
                using HttpClient cyclicHttpClient = PrepareHttpClient(accessToken);
                HttpResponseMessage cyclicResult = await cyclicHttpClient.GetAsync(url);
                
                if (!cyclicResult.IsSuccessStatusCode)
                {
                    throw new EBayApiException((int)cyclicResult.StatusCode);
                }
                
                string cyclicContent = await cyclicResult.Content.ReadAsStringAsync();
                
                GetReturnPoliciesResponseModel cyclicModel = JsonConvert.DeserializeObject<GetReturnPoliciesResponseModel>(cyclicContent);
                
                foreach(ReturnPolicyModel policyModel in cyclicModel.ReturnPolicies)
                {
                    policies.Add(new ReturnPolicyDto
                    {
                        Id = policyModel.ReturnPolicyId,
                        Description = policyModel.Description,
                        Name = policyModel.Name
                    });
                }
                url = cyclicModel.Next;
            }

            return policies;
        }

        private string PrepareUrl(string context, bool isTestUser, string marketPlace)
            => $"https://api{(isTestUser ? ".sandbox" : string.Empty)}.ebay.com/sell/account/v1/{context}_policy?marketplace_id={marketPlace}";
        
        private HttpClient PrepareHttpClient(string accessToken)
        {
            HttpClient client = _httpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            return client;
        }
    }
}