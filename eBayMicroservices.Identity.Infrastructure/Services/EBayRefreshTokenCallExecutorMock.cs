﻿using System.Threading.Tasks;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Application.Services.Responses;
using eBayMicroservices.Identity.Infrastructure.Services.Responses;

namespace eBayMicroservices.Identity.Infrastructure.Services
{
    public class EBayRefreshTokenCallExecutorMock : IEBayRefreshTokenCallExecutor
    {
#pragma warning disable 1998
        public async Task<IEBayRefreshTokenCallResponse> CreateFirstTokens(string code,bool isTestUser)

        {
            return new EBayRefreshTokenCallResponse("testAccessToken","1234","refreshToken");
        }

        public async Task<IEBayRefreshTokenCallResponse> RefreshTokens(string refreshToken,bool isTestUser)
        {
            return new EBayRefreshTokenCallResponse("testAccessToken","1234");
        }
#pragma warning restore 1998
    }
}