﻿using Newtonsoft.Json;

namespace eBayMicroservices.Identity.Infrastructure.Services.Models
{
    public class ReturnPolicyModel
    {
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("returnPolicyId")]
        public string ReturnPolicyId { get; set; }
    }
}