﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace eBayMicroservices.Identity.Infrastructure.Services.Models
{
    public class GetPaymentPoliciesResponseModel
    {
        [JsonProperty("paymentPolicies")]
        public IEnumerable<PaymentPolicyModel> PaymentPolicies { get; set; }
        [JsonProperty("next")]
        public string Next { get; set; }
    }
}