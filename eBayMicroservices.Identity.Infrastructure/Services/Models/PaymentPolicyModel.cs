﻿using Newtonsoft.Json;

namespace eBayMicroservices.Identity.Infrastructure.Services.Models
{
    public class PaymentPolicyModel
    {
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("paymentPolicyId")]
        public string PaymentPolicyId { get; set; }
    }
}