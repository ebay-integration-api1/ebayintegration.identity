﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace eBayMicroservices.Identity.Infrastructure.Services.Models
{
    public class GetReturnPoliciesResponseModel
    {
        [JsonProperty("returnPolicies")]
        public IEnumerable<ReturnPolicyModel> ReturnPolicies { get; set; }
        [JsonProperty("next")]
        public string Next { get; set; }
    }
}