﻿// ReSharper disable InconsistentNaming
namespace eBayMicroservices.Identity.Infrastructure.Services.Models
{
    public class EBayTokenResponseModel
    {
        public string access_token { get; set; }
        public string expires_in { get; set; }
        public string refresh_token { get; set; }
    }
}