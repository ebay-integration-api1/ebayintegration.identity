﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace eBayMicroservices.Identity.Infrastructure.Services.Models
{
    public abstract class EBayBaseRequestModel
    {
        protected abstract IEnumerable<string> Parameters { get; }
        
        public HttpContent AsEBayRequest() => new StringContent(string.Join("&",Parameters),Encoding.UTF8);  
    }
}