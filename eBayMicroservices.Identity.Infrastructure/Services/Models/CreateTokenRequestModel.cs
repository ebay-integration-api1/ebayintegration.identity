﻿using System.Collections.Generic;

namespace eBayMicroservices.Identity.Infrastructure.Services.Models
{
    public class CreateTokenRequestModel : EBayBaseRequestModel
    {
        private static string GrantType => "authorization_code";
        public string Code { get; set; }
        public string RedirectUri { get; set; }
    

        protected override IEnumerable<string> Parameters =>
            new List<string>
            {
                $"grant_type={GrantType}",
                $"code={Code}",
                $"redirect_uri={RedirectUri}"
            };
    }
}