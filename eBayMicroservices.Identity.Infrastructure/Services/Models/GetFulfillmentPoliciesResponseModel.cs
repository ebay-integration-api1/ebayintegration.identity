﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace eBayMicroservices.Identity.Infrastructure.Services.Models
{
    public class GetFulfillmentPoliciesResponseModel
    {
        [JsonProperty("fulfillmentPolicies")]
        public IEnumerable<FulfillmentPolicyModel> FulfillmentPolicies { get; set; }
        [JsonProperty("next")]
        public string Next { get; set; }
    }
}