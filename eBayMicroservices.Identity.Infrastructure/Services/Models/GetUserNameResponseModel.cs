﻿using Newtonsoft.Json;

namespace eBayMicroservices.Identity.Infrastructure.Services.Models
{
    public class GetUserNameResponseModel
    {
        [JsonProperty("username")]
        public string Username { get; set; }
    }
}