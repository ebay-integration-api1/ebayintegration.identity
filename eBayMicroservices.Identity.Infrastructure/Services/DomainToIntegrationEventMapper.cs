﻿using System.Collections.Generic;
using System.Linq;
using Convey.CQRS.Events;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Core.Events.Abstract;

namespace eBayMicroservices.Identity.Infrastructure.Services
{
    public class DomainToIntegrationEventMapper : IDomainToIntegrationEventMapper
    {
        public IEnumerable<IEvent> MapAll(IEnumerable<IDomainEvent> events) => events.Select(Map);

        public IEvent Map(IDomainEvent @event) => @event switch
        {
            _ => null
        };
    }
}