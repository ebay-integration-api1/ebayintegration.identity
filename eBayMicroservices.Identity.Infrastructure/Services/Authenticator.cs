﻿using System;
using eBayMicroservices.Identity.Application.Exceptions.Concrete;
using eBayMicroservices.Identity.Application.Services;
using Microsoft.AspNetCore.Http;

namespace eBayMicroservices.Identity.Infrastructure.Services
{
    public class Authenticator : IAuthenticator
    {
        private readonly IIdentityProvider _identityProvider;

        public Authenticator(IIdentityProvider identityProvider)
        {
            _identityProvider = identityProvider;
        }
        
        public void Authenticate(HttpContext ctx)
        {
            string identity = _identityProvider.GetIdentityAsString(ctx);

            if (!Guid.TryParse(identity, out Guid _))
            {
                throw new UnauthorisedUserException();
            }
        }
    }
}