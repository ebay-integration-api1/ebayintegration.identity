﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Application.Options;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Application.Services.Responses;
using eBayMicroservices.Identity.Infrastructure.Services.Models;
using eBayMicroservices.Identity.Infrastructure.Services.Responses;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace eBayMicroservices.Identity.Infrastructure.Services
{
    public class EBayRefreshTokenCallExecutor : IEBayRefreshTokenCallExecutor
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly string _url;
        private readonly ILogger<EBayRefreshTokenCallExecutor> _logger;
        private readonly IBaseEncodeConverter _baseEncodeConverter;
        private readonly EBayDevelopConfig _config;

        public EBayRefreshTokenCallExecutor(IHttpClientFactory httpClientFactory,EBayDevelopConfig config, ILogger<EBayRefreshTokenCallExecutor> logger, IBaseEncodeConverter baseEncodeConverter)
        {
            _httpClientFactory = httpClientFactory;
            _logger = logger;
            _baseEncodeConverter = baseEncodeConverter;
            _url = "https://api.sandbox.ebay.com/identity/v1/oauth2/token";
            _config = config;
        }

        public async Task<IEBayRefreshTokenCallResponse> CreateFirstTokens(string code,bool isTestUser)
        {
            HttpContent requestContent = PrepareRequestToCreateTokens(code,isTestUser);

            return await SendRequestAsync(requestContent,isTestUser);
        }
        
        public async Task<IEBayRefreshTokenCallResponse> RefreshTokens(string refreshToken,bool isTestUser)
        {
            HttpContent content = PrepareRequestToRefreshTokens(refreshToken);
            
            return await SendRequestAsync(content,isTestUser);
        }
        

        private async Task<IEBayRefreshTokenCallResponse> SendRequestAsync(HttpContent content,bool isTestUser)
        {
            using HttpClient httpClient = _httpClientFactory.CreateClient();

            string clientId = isTestUser ? _config.Sandbox.AppId : _config.Prod.AppId;

            string clientHash = isTestUser ? _config.Sandbox.CertId : _config.Prod.CertId;
            
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",_baseEncodeConverter.Encode($"{clientId}:{clientHash}"));
            
            HttpResponseMessage res = await httpClient.PostAsync(_url, content);
            if (!res.IsSuccessStatusCode)
            {
                var errorResult = await res.Content.ReadAsStringAsync();
                _logger.Log(LogLevel.Error,errorResult);
                return new EBayRefreshTokenCallResponse();
            }
            EBayTokenResponseModel result = JsonConvert.DeserializeObject<EBayTokenResponseModel>(await res.Content.ReadAsStringAsync());
            return new EBayRefreshTokenCallResponse(result.access_token,result.expires_in,result.refresh_token);
        }
        
        private HttpContent PrepareRequestToCreateTokens(string code,bool isTestUser)
        {
            CreateTokenRequestModel createTokenRequestModel = new CreateTokenRequestModel
            {
                Code = code,
                RedirectUri = isTestUser? _config.Sandbox.RedirectUrl : _config.Prod.RedirectUrl
            };

            HttpContent content = createTokenRequestModel.AsEBayRequest();
            
            SetContentMediaType(content);
            return content;
        }
        private static HttpContent PrepareRequestToRefreshTokens(string refreshToken)
        {
            RefreshTokenRequestModel refreshTokenRequestModel = new RefreshTokenRequestModel
            {
                RefreshToken = refreshToken
            };

            HttpContent content = refreshTokenRequestModel.AsEBayRequest();
            
            SetContentMediaType(content);
            return content;
        }

        private static void SetContentMediaType(HttpContent content)
        {
            content.Headers.ContentType = new MediaTypeHeaderValue(ApplicationMediaType);
        }

        private static string ApplicationMediaType => "application/x-www-form-urlencoded";
    }
}