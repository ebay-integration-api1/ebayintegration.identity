﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;  
using System.IdentityModel.Tokens.Jwt;
using eBayMicroservices.Identity.Application.Commands;
using eBayMicroservices.Identity.Application.Exceptions.Concrete;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Core.Entities;
using eBayMicroservices.Identity.Core.Exceptions;
using eBayMicroservices.Identity.Core.Repositories;
using Microsoft.Extensions.Logging;

namespace eBayMicroservices.Identity.Infrastructure.Services
{
    public class IdentityService : IIdentityService
    {
        private const string Secret = "ThisismySecretKey";
        private readonly IUserRepository _userRepository;
        private readonly ILogger<IdentityService> _logger;
        public IdentityService(IUserRepository userRepository, ILogger<IdentityService> logger)
        {
            _userRepository = userRepository;
            _logger = logger;
        }

        public async Task<string> SignInAsync(SignIn command)
        {

            if (string.IsNullOrWhiteSpace(command.Login))
            {
                throw new InvalidUserDataException(nameof(command.Login));
            }
            if (string.IsNullOrWhiteSpace(command.Password))
            {
                throw new InvalidUserDataException(nameof(command.Password));
            }
            
            User user = await _userRepository.GetUserByLogin(command.Login);

            if (user is null)
            {
                throw new UserNotFoundException(command.Login);
            }
            
            if (user.Password != command.Password)
            {
                throw new InvalidUserDataException(nameof(command.Password));
            }
            
            SymmetricSecurityKey securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Secret));    
            
            SigningCredentials credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);    
    
            Claim[] claims = new[] {
                new Claim(UserId, user.Id.Value.ToString()),    
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())    
            };


            JwtSecurityToken jwt = new JwtSecurityToken("issuer", "issuer", claims, expires: DateTime.Now.AddHours(3),
                signingCredentials: credentials);
            
            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }

        private const string UserId = "UserId";
        public Guid? ValidateUserByToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var validationParameters = GetValidationParameters();
            try
            {
                ClaimsPrincipal principal =
                    tokenHandler.ValidateToken(token, validationParameters, out SecurityToken _);

                ClaimsIdentity claimer = principal.Identity as ClaimsIdentity;

                Claim claim = claimer?.Claims.FirstOrDefault(x => x.Type == UserId);

                if (claim is null) return null;

                return Guid.Parse(claim.Value);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error,ex,ex.Message);
                return Guid.Empty;
            }

        }
        private static TokenValidationParameters GetValidationParameters()
        {
            return new TokenValidationParameters()
            {
                ValidateLifetime = false, // Because there is no expiration in the generated token
                ValidateAudience = false, // Because there is no audiance in the generated token
                ValidateIssuer = false,   // Because there is no issuer in the generated token
                ValidIssuer = "issuer",
                ValidAudience = "issuer",
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Secret)) // The same key as the one that generate the token
            };
        }
    }
}