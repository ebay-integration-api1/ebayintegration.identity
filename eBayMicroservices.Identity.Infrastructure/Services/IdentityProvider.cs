﻿using System;
using System.Linq;
using eBayMicroservices.Identity.Application.Services;
using Microsoft.AspNetCore.Http;

namespace eBayMicroservices.Identity.Infrastructure.Services
{
    public class IdentityProvider : IIdentityProvider
    {
        public Guid GetIdentity(HttpContext ctx) 
            => Guid.Parse(GetIdentityAsString(ctx));

        public string GetIdentityAsString(HttpContext ctx) 
            => ctx.Request.Headers["Authorization"].FirstOrDefault();

    }
}