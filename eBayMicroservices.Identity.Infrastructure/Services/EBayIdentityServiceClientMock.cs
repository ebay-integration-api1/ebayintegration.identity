﻿using System.Threading.Tasks;
using eBayMicroservices.Identity.Application.Services;

namespace eBayMicroservices.Identity.Infrastructure.Services
{
    public class EBayIdentityServiceClientMock : IEBayIdentityServiceClient
    {
        public Task<string> GetUserName(string userAccessToken)
        {
            return Task.FromResult("mockedUserName");
        }
    }
}