﻿using eBayMicroservices.Identity.Application.Services.Responses;

namespace eBayMicroservices.Identity.Infrastructure.Services.Responses
{
    public class EBayRefreshTokenCallResponse : IEBayRefreshTokenCallResponse
    {
        public EBayRefreshTokenCallResponse()
        {
            Success = false;
        }

        public EBayRefreshTokenCallResponse(string accessToken, string expiresIn, string refreshToken = null)
        {
            Success = true;
            AccessToken = accessToken;
            ExpiresIn = expiresIn;
            RefreshToken = refreshToken;
        }
        public bool Success { get; }
        public string AccessToken { get; }
        public string RefreshToken { get; }
        public string ExpiresIn { get; }
    }
}