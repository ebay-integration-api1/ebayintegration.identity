﻿using System;
using System.Threading;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Application.Services.Responses;
using eBayMicroservices.Identity.Core.Entities;
using eBayMicroservices.Identity.Core.Repositories;
using Microsoft.Extensions.Logging;

namespace eBayMicroservices.Identity.Infrastructure.Services
{
    public class TokenRefreshExecutor : ITokenRefreshExecutor
    {
        private readonly IUserRepository _userRepository;
        private readonly IEBayRefreshTokenCallExecutor _eBayRefreshTokenCallExecutor;
        private readonly ILogger<TokenRefreshExecutor> _logger;

        public TokenRefreshExecutor(IUserRepository userRepository, IEBayRefreshTokenCallExecutor ebayRefreshTokenCallExecutor, ILogger<TokenRefreshExecutor> logger)
        {
            _userRepository = userRepository;
            _eBayRefreshTokenCallExecutor = ebayRefreshTokenCallExecutor;
            _logger = logger;
        }

        public async Task ExecuteTokenRefresh(User user)
        {
            try
            {
                await CreateNewTokens(user);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error,ex,ex.Message);
            }
        }
        
        
        
        public async Task ExecuteTokenRefresh()
        {
            while (true)
            {
                User user = null;
                try
                {
                    user = await _userRepository.GetUserToRefreshToken();
                    if (user is null) break;

                    Thread.Sleep(1000);
                    
                    if (UserHasNoToken(user))
                    {
                        await CreateNewTokens(user);
                        continue;
                    }

                    await RefreshExistingTokens(user);
                }
                catch (Exception ex)
                {
                    if (user is {})
                    {
                        await _userRepository.UpdateUserTokensAsInvalid(user.Id.Value);
                    }
                    _logger.Log(LogLevel.Error,ex,ex.Message);
                    break;
                }
            }
        }

        private async Task RefreshExistingTokens(User user)
        {
            IEBayRefreshTokenCallResponse result = await _eBayRefreshTokenCallExecutor.RefreshTokens(user.RefreshToken,user.IsTestUser);
            if (result.Success)
            {
                await _userRepository.UpdateUserTokens(user.Id.Value, result.AccessToken, result.ExpiresIn);
            }
            else
            {
                await _userRepository.UpdateUserTokensAsInvalid(user.Id.Value);
            }

        }

        private async Task CreateNewTokens(User user)
        {
            IEBayRefreshTokenCallResponse result = await _eBayRefreshTokenCallExecutor.CreateFirstTokens(user.AuthenticationCode,user.IsTestUser);
            if (result.Success)
            {
                await _userRepository.UpdateUserTokens(user.Id.Value, result.AccessToken,result.RefreshToken, result.ExpiresIn);
            }
            else
            {
                await _userRepository.UpdateUserTokensAsInvalid(user.Id.Value);
            }
        }

        private static bool UserHasNoToken(User user)
        {
            return !user.SetTokenDateTime.HasValue;
        }
    }
}