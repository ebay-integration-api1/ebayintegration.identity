﻿using eBayMicroservices.Identity.Application.Services;

namespace eBayMicroservices.Identity.Infrastructure.Services
{
    public class BaseEncodeConverter : IBaseEncodeConverter
    {
        public string Encode(string toConvert)
        {
            byte[] plainTextBytes = System.Text.Encoding.UTF8.GetBytes(toConvert);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public string Decode(string toConvert)
        {
            byte[] base64EncodedBytes = System.Convert.FromBase64String(toConvert);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}