﻿using System.Collections.Generic;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Application.DTOs.Policies;
using eBayMicroservices.Identity.Application.Services;

namespace eBayMicroservices.Identity.Infrastructure.Services
{
    public class EBayPoliciesServiceClientMock : IEBayPoliciesServiceClient
    {
#pragma warning disable 1998
        public async Task<IEnumerable<FulfillmentPolicyDto>> GetFulfillmentPolicies(string marketplace, string accessToken,bool isTest)

        {
            return new List<FulfillmentPolicyDto>
            {
                new FulfillmentPolicyDto
                {
                    Description = "Description",
                    Id = "Id",
                    Name = "Name"
                }
            };
        }

        public async Task<IEnumerable<PaymentPolicyDto>> GetPaymentPolicies(string marketplace, string accessToken,bool isTest)
        {
            return new List<PaymentPolicyDto>
            {
                new PaymentPolicyDto
                {
                    Description = "Description",
                    Id = "Id",
                    Name = "Name"
                }
            };
        }

        public async Task<IEnumerable<ReturnPolicyDto>> GetReturnPolicies(string marketplace, string accessToken,bool isTest)
        {
            return new List<ReturnPolicyDto>
            {
                new ReturnPolicyDto
                {
                    Description = "Description",
                    Id = "Id",
                    Name = "Name"
                }
            };
        }
#pragma warning restore 1998
    }
}