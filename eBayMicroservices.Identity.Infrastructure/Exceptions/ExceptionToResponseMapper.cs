﻿using System;
using System.Net;
using Convey.WebApi.Exceptions;
using eBayMicroservices.Identity.Application.Exceptions.Abstract;
using eBayMicroservices.Identity.Application.Exceptions.Concrete;
using eBayMicroservices.Identity.Core.Exceptions.Abstract;

namespace eBayMicroservices.Identity.Infrastructure.Exceptions
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class ExceptionToResponseMapper : IExceptionToResponseMapper
    {
        public ExceptionResponse Map(Exception exception)
            => exception switch
            {
                DomainException ex => new ExceptionResponse(new {code = ex.Code, reason = ex.Message},
                    HttpStatusCode.BadRequest),

                AppException ex => ex switch
                {
                    UnauthorisedUserException unauthorisedUserException
                        => new ExceptionResponse(
                            new
                            {
                                code = unauthorisedUserException.Code,
                                reason = unauthorisedUserException.Message
                            },
                            HttpStatusCode.Unauthorized),
                    UserNotFoundException userNotFoundException
                        => new ExceptionResponse(
                            new
                            {
                                code = userNotFoundException.Code,
                                reason = userNotFoundException.Message
                            },
                            HttpStatusCode.NotFound),
                    UserAlreadyExists userAlreadyExists
                        => new ExceptionResponse(
                            new
                            {
                                code = userAlreadyExists.Code,
                                reason = userAlreadyExists.Message
                            },
                            HttpStatusCode.Conflict),
                    NotExistingMarketplaceWhileSigningUpUser notExistingMarketplaceWhileSigningUpUser
                        =>
                        new ExceptionResponse(new
                        {
                            code = notExistingMarketplaceWhileSigningUpUser.Code,
                            reason = notExistingMarketplaceWhileSigningUpUser.Message
                        }, HttpStatusCode.BadRequest),
                    EmptyMarketplaceException emptyMarketplaceException => new ExceptionResponse(new
                    {
                        code = emptyMarketplaceException.Code,
                        reason = emptyMarketplaceException.Message
                    }, HttpStatusCode.BadRequest),
                    EBayEnvironmentNotSpecified eBayEnvironmentNotSpecified => new ExceptionResponse(new
                        {
                            code = eBayEnvironmentNotSpecified.Code,
                            reason = eBayEnvironmentNotSpecified.Message
                        }, HttpStatusCode.BadRequest)
                    ,
                    EBayApiException eBayApiException => new ExceptionResponse(new
                    {
                        code = eBayApiException.Code,
                        reason = eBayApiException.Message
                    }, HttpStatusCode.FailedDependency),
                    _ => new ExceptionResponse(
                        new
                        {
                            code = "error",
                            reason = "There was an error."
                        },
                        HttpStatusCode.BadRequest)
                },

                _ => new ExceptionResponse(new
                    {
                        code = "error", reason = "There was an error."
                    },
                    HttpStatusCode.BadRequest)
            };
    }
}