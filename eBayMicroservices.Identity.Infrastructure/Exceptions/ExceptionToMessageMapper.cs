﻿using System;
using Convey.MessageBrokers.RabbitMQ;

namespace eBayMicroservices.Identity.Infrastructure.Exceptions
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class ExceptionToMessageMapper : IExceptionToMessageMapper
    {
        public object Map(Exception exception, object message)
            => exception switch
            {
                _ => null
            };
    }
}