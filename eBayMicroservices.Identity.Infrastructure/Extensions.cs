﻿using System;
using Convey;
using Convey.CQRS.Queries;
using Convey.Discovery.Consul;
using Convey.HTTP;
using Convey.LoadBalancing.Fabio;
using Convey.MessageBrokers.Outbox;
using Convey.MessageBrokers.RabbitMQ;
using Convey.Metrics.AppMetrics;
using Convey.Persistence.MongoDB;
using Convey.WebApi;
using Convey.WebApi.Exceptions;
using eBayMicroservices.Identity.Application.Events.Handlers;
using eBayMicroservices.Identity.Application.Options;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Core.Repositories;
using eBayMicroservices.Identity.Infrastructure.Exceptions;
using eBayMicroservices.Identity.Infrastructure.Mongo.CollectionProviding.Abstract;
using eBayMicroservices.Identity.Infrastructure.Mongo.CollectionProviding.Concrete;
using eBayMicroservices.Identity.Infrastructure.Mongo.Documents.Abstract;
using eBayMicroservices.Identity.Infrastructure.Mongo.Documents.Concrete;
using eBayMicroservices.Identity.Infrastructure.Mongo.Repositories;
using eBayMicroservices.Identity.Infrastructure.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using IAuthenticator = eBayMicroservices.Identity.Application.Services.IAuthenticator;

namespace eBayMicroservices.Identity.Infrastructure
{
    public static class Extensions
    {
        public static IConveyBuilder AddInfrastructure(this IConveyBuilder builder)
        {
            builder
                .AddQueryHandlers()
                .AddInMemoryQueryDispatcher()
                .AddHttpClient()
                .AddErrorHandler<ExceptionToResponseMapper>()
                .AddExceptionToMessageMapper<ExceptionToMessageMapper>()
                .AddRabbitMq()
                .AddMongo()
                .AddConsul()
                .AddFabio()
                .AddMessageOutbox()
                .AddMetrics();

            // ReSharper disable once RedundantTypeArgumentsOfMethod
            // ReSharper disable once RedundantCast
            builder.Services.Configure<KestrelServerOptions>((Action<KestrelServerOptions>) (o => o.AllowSynchronousIO = true));
            // ReSharper disable once RedundantTypeArgumentsOfMethod
            // ReSharper disable once RedundantCast
            builder.Services.Configure<IISServerOptions>((Action<IISServerOptions>) (o => o.AllowSynchronousIO = true));

            IServiceCollection services = builder.Services;
            
            services.AddTransient<IAuthenticator,Authenticator>();
            services.AddTransient<IDomainToIntegrationEventMapper,DomainToIntegrationEventMapper>();
            services.AddTransient<IEBayRefreshTokenCallExecutor,EBayRefreshTokenCallExecutor>();
            services.AddTransient<IEventProcessor,EventProcessor>();
            services.AddTransient<IIdentityProvider,IdentityProvider>();
            services.AddTransient<IIdentityService,IdentityService>();
            services.AddTransient<IMessageBroker,MessageBroker>();
            services.AddTransient<ITokenRefreshExecutor,TokenRefreshExecutor>();
            services.AddTransient<IUserRepository,UserRepository>();
            services.AddTransient<IObjectDocumentMapper,ObjectDocumentMapper>();
            services.AddTransient<IMongoCollectionProvider,MongoCollectionProvider>();
            services.AddTransient<IBaseEncodeConverter,BaseEncodeConverter>();
            services.AddTransient<IMarketplaceRepository,MarketplaceRepository>();
            services.AddTransient<IRedirectUriToAuthorizeInEBayProvider,RedirectUriToAuthorizeInEBayProvider>();

            if (IsGitlabRunner() || IsNotK8SConfig())
            {
                services.AddTransient<IEBayRefreshTokenCallExecutor, EBayRefreshTokenCallExecutorMock>();
                services.AddTransient<IEBayIdentityServiceClient, EBayIdentityServiceClientMock>();
                services.AddTransient<IEBayPoliciesServiceClient, EBayPoliciesServiceClientMock>();
                
            }
            else
            {
                services.AddTransient<IEBayRefreshTokenCallExecutor, EBayRefreshTokenCallExecutor>();
                services.AddTransient<IEBayIdentityServiceClient, EBayIdentityServiceClient>();
                services.AddTransient<IEBayPoliciesServiceClient, EBayPoliciesServiceClient>();
            }

            services.AddSingleton<IExceptionToResponseMapper, ExceptionToResponseMapper>();

            ServiceProvider serviceProvider = services.BuildServiceProvider();

            IConfiguration configuration = serviceProvider.GetService<IConfiguration>();

            EBayDevelopConfig eBayDevelopConfig = configuration.GetSection("EBayDevelopConfig").Get<EBayDevelopConfig>();

            services.AddSingleton(eBayDevelopConfig);

            builder.Services.Scan(s => s.FromAssemblies(AppDomain.CurrentDomain.GetAssemblies())
                .AddClasses(c => c.AssignableTo(typeof(IDomainEventHandler<>)))
                .AsImplementedInterfaces().WithTransientLifetime());
            
            return builder;
        }

        private static bool IsNotK8SConfig()
        {
            string env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")?.ToLower();
            if (env is null) return true;
            return !env.Contains("kubernetes");
        }

        private static bool IsGitlabRunner()
        {
            return Environment.GetEnvironmentVariable("RUNNER") == "linux";
        }


        public static IApplicationBuilder UseInfrastructure(this IApplicationBuilder app)
        {
            app
                .UseErrorHandler()
                .UseConvey()
                .UseMetrics()
                //.UsePublicContracts<ContractAttribute>()
                .UseRabbitMq();
            
            //.SubscribeEvent<OrderAdded>()

            return app;
        }
    }
}