﻿using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Core.Repositories;

namespace eBayMicroservices.Identity.Infrastructure.Mongo.Queries.Handlers
{
    public abstract class GetPoliciesHandlerBase
    {
        protected readonly IUserRepository UserRepository;
        protected readonly IEBayPoliciesServiceClient EbayPoliciesServiceClient;

        protected GetPoliciesHandlerBase(IUserRepository userRepository, IEBayPoliciesServiceClient ebayPoliciesServiceClient)
        {
            UserRepository = userRepository;
            EbayPoliciesServiceClient = ebayPoliciesServiceClient;
        }
    }
}