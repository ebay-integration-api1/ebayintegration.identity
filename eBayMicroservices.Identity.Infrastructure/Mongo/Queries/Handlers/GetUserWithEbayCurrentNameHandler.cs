﻿using System.Threading.Tasks;
using Convey.CQRS.Queries;
using eBayMicroservices.Identity.Application.DTOs;
using eBayMicroservices.Identity.Application.Queries;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Infrastructure.Mongo.CollectionProviding.Abstract;
using eBayMicroservices.Identity.Infrastructure.Mongo.Documents;
using MongoDB.Driver;

namespace eBayMicroservices.Identity.Infrastructure.Mongo.Queries.Handlers
{
    public class GetUserWithEbayCurrentNameHandler : IQueryHandler<GetUserWithEbayCurrentName,UserDto>
    {
        private readonly IMongoCollectionProvider _mongoCollectionProvider;
        private readonly IEBayIdentityServiceClient _eBayIdentityServiceClient;

        public GetUserWithEbayCurrentNameHandler(IMongoCollectionProvider mongoCollectionProvider, IEBayIdentityServiceClient eBayIdentityServiceClient)
        {
            _mongoCollectionProvider = mongoCollectionProvider;
            _eBayIdentityServiceClient = eBayIdentityServiceClient;
        }

        public async Task<UserDto> HandleAsync(GetUserWithEbayCurrentName query)
        {
            UserDocument resource = await _mongoCollectionProvider.UserDocumentCollection()
                .Find(x => x.Id == query.Id)
                .SingleOrDefaultAsync();
            if (resource is null)
            {
                return null;
            }

            string actualEbayName = await _eBayIdentityServiceClient.GetUserName(resource.AccessToken);
            
            UserDto dto = new UserDto
            {
                Id = resource.Id,
                Login = resource.Login,
                Name = actualEbayName,
                Marketplace = resource.Marketplace,
                AccessToken = resource.AccessToken,
                IsTestUser = resource.IsTestUser
            };

            return dto;
        }
    }
}