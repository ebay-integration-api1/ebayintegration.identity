﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Convey.CQRS.Queries;
using eBayMicroservices.Identity.Application.DTOs.Policies;
using eBayMicroservices.Identity.Application.Exceptions.Concrete;
using eBayMicroservices.Identity.Application.Queries;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Core.Entities;
using eBayMicroservices.Identity.Core.Repositories;

namespace eBayMicroservices.Identity.Infrastructure.Mongo.Queries.Handlers
{
    public class GetFulfillmentPoliciesHandler : GetPoliciesHandlerBase,IQueryHandler<GetFulfillmentPolicies,IEnumerable<FulfillmentPolicyDto>>
    {
        public GetFulfillmentPoliciesHandler(IUserRepository userRepository, IEBayPoliciesServiceClient ebayPoliciesServiceClient) : base(userRepository,ebayPoliciesServiceClient)
        {
        }

        public async Task<IEnumerable<FulfillmentPolicyDto>> HandleAsync(GetFulfillmentPolicies query)
        {
            if (string.IsNullOrWhiteSpace(query.Marketplace))
            {
                throw new EmptyMarketplaceException();
            }

            User userDto = await UserRepository.GetUserById(query.UserId);

            if (userDto is null)
            {
                throw new UserNotFoundException(query.UserId.ToString());
            }

            IEnumerable<FulfillmentPolicyDto> policies = await EbayPoliciesServiceClient.GetFulfillmentPolicies(query.Marketplace, userDto.AccessToken,userDto.IsTestUser);
            
            return policies;
        }
    }
}