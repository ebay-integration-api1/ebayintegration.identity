﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Convey.CQRS.Queries;
using eBayMicroservices.Identity.Application.DTOs;
using eBayMicroservices.Identity.Application.Queries;
using eBayMicroservices.Identity.Infrastructure.Mongo.CollectionProviding.Abstract;
using eBayMicroservices.Identity.Infrastructure.Mongo.Documents;
using MongoDB.Driver;

namespace eBayMicroservices.Identity.Infrastructure.Mongo.Queries.Handlers
{
    public class GetMarketplacesHandler : IQueryHandler<GetMarketplaces,IEnumerable<MarketplaceDto>>
    {
        private readonly IMongoCollectionProvider _mongoCollectionProvider;

        public GetMarketplacesHandler(IMongoCollectionProvider mongoCollectionProvider)
        {
            _mongoCollectionProvider = mongoCollectionProvider;
        }

        public async Task<IEnumerable<MarketplaceDto>> HandleAsync(GetMarketplaces query)
        {
            IEnumerable<MarketplaceDocument> marketplaces= await _mongoCollectionProvider.MarketplaceDocumentCollection()
                .Find(x => true)
                .ToListAsync();
            if (marketplaces is null)
            {
                return new List<MarketplaceDto>();
            }

            return marketplaces.Select(x=> new MarketplaceDto
            {
                Code = x.Code,
                Name = x.Name
            });
        }
    }
}