﻿using System.Threading.Tasks;
using Convey.CQRS.Queries;
using eBayMicroservices.Identity.Application.DTOs;
using eBayMicroservices.Identity.Application.Queries;
using eBayMicroservices.Identity.Infrastructure.Mongo.CollectionProviding.Abstract;
using eBayMicroservices.Identity.Infrastructure.Mongo.Documents;
using MongoDB.Driver;

namespace eBayMicroservices.Identity.Infrastructure.Mongo.Queries.Handlers
{
    public class GetUserHandler : IQueryHandler<GetUser, UserDto>
    {
        private readonly IMongoCollectionProvider _mongoCollectionProvider;

        public GetUserHandler(IMongoCollectionProvider mongoCollectionProvider)
        {
            _mongoCollectionProvider = mongoCollectionProvider;
        }

        public async Task<UserDto> HandleAsync(GetUser query)
        {
            UserDocument resource = await _mongoCollectionProvider.UserDocumentCollection()
                .Find(x => x.Id == query.Id)
                .SingleOrDefaultAsync();
            if (resource is null)
            {
                return null;
            }

            return new UserDto
            {
                Id = resource.Id,
                Login = resource.Login,
                Name = resource.Name,
                Marketplace = resource.Marketplace,
                AccessToken = resource.AccessToken,
                IsTestUser = resource.IsTestUser
            };
        }
    }
}