﻿using System.Threading.Tasks;
using Convey.CQRS.Queries;
using eBayMicroservices.Identity.Application.Exceptions.Concrete;
using eBayMicroservices.Identity.Application.Queries;
using eBayMicroservices.Identity.Application.Services;

namespace eBayMicroservices.Identity.Infrastructure.Mongo.Queries.Handlers
{
    public class GetUrlToSignInHandler : IQueryHandler<GetUrlToSignIn,string>
    {
        private readonly IRedirectUriToAuthorizeInEBayProvider _redirectUriToAuthorizeInEBayProvider;
        public GetUrlToSignInHandler(IRedirectUriToAuthorizeInEBayProvider eBayCommonCredentialsHolder)
        {
            _redirectUriToAuthorizeInEBayProvider = eBayCommonCredentialsHolder;
        }
#pragma warning disable 1998
        public async Task<string> HandleAsync(GetUrlToSignIn query)
        {
            if (!query.IsSandbox.HasValue)
            {
                throw new EBayEnvironmentNotSpecified();
            }
            return _redirectUriToAuthorizeInEBayProvider.UrlToEBayAuthorize(query.IsSandbox.Value);
        }
#pragma warning restore 1998
    }
}