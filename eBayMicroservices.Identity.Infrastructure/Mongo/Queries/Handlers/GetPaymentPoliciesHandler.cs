﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Convey.CQRS.Queries;
using eBayMicroservices.Identity.Application.DTOs.Policies;
using eBayMicroservices.Identity.Application.Exceptions.Concrete;
using eBayMicroservices.Identity.Application.Queries;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Core.Entities;
using eBayMicroservices.Identity.Core.Repositories;

namespace eBayMicroservices.Identity.Infrastructure.Mongo.Queries.Handlers
{
    public class GetPaymentPoliciesHandler : GetPoliciesHandlerBase,IQueryHandler<GetPaymentPolicies,IEnumerable<PaymentPolicyDto>>
    {
        public GetPaymentPoliciesHandler(IUserRepository userRepository, IEBayPoliciesServiceClient ebayPoliciesServiceClient) : base(userRepository,ebayPoliciesServiceClient)
        {
        }

        public async Task<IEnumerable<PaymentPolicyDto>> HandleAsync(GetPaymentPolicies query)
        {
            if (string.IsNullOrWhiteSpace(query.Marketplace))
            {
                throw new EmptyMarketplaceException();
            }

            User userDto = await UserRepository.GetUserById(query.UserId);

            if (userDto is null)
            {
                throw new UserNotFoundException(query.UserId.ToString());
            }

            IEnumerable<PaymentPolicyDto> policies = await EbayPoliciesServiceClient.GetPaymentPolicies(query.Marketplace, userDto.AccessToken,userDto.IsTestUser);
            
            return policies;
        }
    }
}