﻿using System.Threading.Tasks;
using eBayMicroservices.Identity.Core.Repositories;
using eBayMicroservices.Identity.Infrastructure.Mongo.CollectionProviding.Abstract;
using eBayMicroservices.Identity.Infrastructure.Mongo.Documents;
using MongoDB.Driver;

namespace eBayMicroservices.Identity.Infrastructure.Mongo.Repositories
{
    public class MarketplaceRepository : IMarketplaceRepository
    {
        private readonly IMongoCollectionProvider _mongoCollectionProvider;

        public MarketplaceRepository(IMongoCollectionProvider mongoCollectionProvider)
        {
            _mongoCollectionProvider = mongoCollectionProvider;
        }

        public async Task<bool> ExistsAsync(string code)
        {
            IMongoCollection<MarketplaceDocument> marketplaceDocumentCollection = _mongoCollectionProvider.MarketplaceDocumentCollection();

            bool exists = await marketplaceDocumentCollection.Find(x => x.Code == code).AnyAsync();

            return exists;
        }
    }
}