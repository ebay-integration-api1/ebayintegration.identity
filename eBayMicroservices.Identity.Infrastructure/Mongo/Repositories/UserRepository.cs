﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Application.Exceptions.Concrete;
using eBayMicroservices.Identity.Core.Entities;
using eBayMicroservices.Identity.Core.Repositories;
using eBayMicroservices.Identity.Infrastructure.Mongo.CollectionProviding.Abstract;
using eBayMicroservices.Identity.Infrastructure.Mongo.Documents;
using eBayMicroservices.Identity.Infrastructure.Mongo.Documents.Abstract;
using MongoDB.Driver;

namespace eBayMicroservices.Identity.Infrastructure.Mongo.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IObjectDocumentMapper _objectToDocumentMapper;
        private readonly IMongoCollectionProvider _mongoCollectionProvider;

        public UserRepository(IObjectDocumentMapper objectToDocumentMapper, IMongoCollectionProvider mongoCollection)
        {
            _objectToDocumentMapper = objectToDocumentMapper;
            _mongoCollectionProvider = mongoCollection;
        }

        public async Task AddAsync(User user)
        {
             await _mongoCollectionProvider.UserDocumentCollection()
                .InsertOneAsync(_objectToDocumentMapper.Convert(user));
        }
        public async Task UpdateUserTokens(Guid id, string accessToken, string expiresIn)
        {
            UserDocument user = await _mongoCollectionProvider.UserDocumentCollection()
                .Find(x => x.Id == id)
                .FirstOrDefaultAsync();
            if(user is null) throw new UserNotFoundException(id.ToString());

            FilterDefinition<UserDocument> filter = Builders<UserDocument>.Filter
                .Eq(s => s.Id, user.Id);
            
            
            UpdateDefinition<UserDocument> update = Builders<UserDocument>.Update
                    .Set(s => s.AccessToken, accessToken)
                    .Set(s=> s.ExpiresIn, int.Parse(expiresIn))
                    .Set(s=> s.SetTokenDateTime,DateTime.UtcNow.Ticks)
                    .Set(s=> s.SetTokenDateTimeString,new DateTime(DateTime.UtcNow.Ticks).ToString(CultureInfo.InvariantCulture))
                    .Set(s=> s.RefreshTokenError,false)
                ;

            await _mongoCollectionProvider.UserDocumentCollection().UpdateOneAsync(filter, update);
        }

        public async Task UpdateUserTokensAsInvalid(Guid id)
        {
            UserDocument user = await _mongoCollectionProvider.UserDocumentCollection()
                .Find(x => x.Id == id)
                .FirstOrDefaultAsync();
            if(user is null) throw new UserNotFoundException(id.ToString());

            FilterDefinition<UserDocument> filter = Builders<UserDocument>.Filter
                .Eq(s => s.Id, user.Id);
            
            
            UpdateDefinition<UserDocument> update = Builders<UserDocument>.Update
                    .Set(s => s.RefreshTokenError, true)
                    .Set(s=> s.SetTokenDateTime,DateTime.UtcNow.Ticks)
                    .Set(s=> s.SetTokenDateTimeString,new DateTime(DateTime.UtcNow.Ticks).ToString(CultureInfo.InvariantCulture))
                ;

            await _mongoCollectionProvider.UserDocumentCollection().UpdateOneAsync(filter, update);
        }

        public async Task UpdateUserTokens(Guid id, string accessToken, string refreshToken, string expiresIn)
        {
            UserDocument user = await _mongoCollectionProvider.UserDocumentCollection()
                .Find(x => x.Id == id)
                .FirstOrDefaultAsync();
            if(user is null) throw new UserNotFoundException(id.ToString());

            FilterDefinition<UserDocument> filter = Builders<UserDocument>.Filter
                .Eq(s => s.Id, user.Id);
            
            
            UpdateDefinition<UserDocument> update = Builders<UserDocument>.Update
                .Set(s => s.AccessToken, accessToken)
                .Set(s=> s.RefreshToken,refreshToken)
                .Set(s=> s.ExpiresIn, int.Parse(expiresIn))
                .Set(s=> s.SetTokenDateTime,DateTime.UtcNow.Ticks)
                .Set(s=> s.SetTokenDateTimeString,new DateTime(DateTime.UtcNow.Ticks).ToString(CultureInfo.InvariantCulture))
                .Set(s=> s.RefreshTokenError,false)
                ;

            await _mongoCollectionProvider.UserDocumentCollection().UpdateOneAsync(filter, update);
        }
        
        public async Task UpdateUserAuthenticationCode(Guid id, string authenticationCode)
        {
            UserDocument user = await _mongoCollectionProvider.UserDocumentCollection()
                .Find(x => x.Id == id)
                .FirstOrDefaultAsync();
            if(user is null) throw new UserNotFoundException(id.ToString());

            FilterDefinition<UserDocument> filter = Builders<UserDocument>.Filter
                .Eq(s => s.Id, user.Id);

            UpdateDefinition<UserDocument> update = Builders<UserDocument>.Update
                .Set(s => s.AuthenticationCode, authenticationCode);

            await _mongoCollectionProvider.UserDocumentCollection().UpdateOneAsync(filter, update);
        }
        

        public async Task<bool> ExistsAsync(string login)
        {
            bool result = await
                _mongoCollectionProvider.UserDocumentCollection()
                    .Find(x => x.Login == login)
                    .AnyAsync();
            return result;
        }

        public async Task<User> GetUserByLogin(string login)
        {
            UserDocument result = await
                _mongoCollectionProvider.UserDocumentCollection()
                    .Find(x => x.Login == login).FirstOrDefaultAsync();
                    
            return _objectToDocumentMapper.Convert(result);
        }
        public async Task<User> GetUserById(Guid id)
        {
            UserDocument result = await
                _mongoCollectionProvider.UserDocumentCollection()
                    .Find(x => x.Id == id).FirstOrDefaultAsync();
                    
            return _objectToDocumentMapper.Convert(result);
        }
        public async Task<User> GetUserToRefreshToken()
        {
            const int safeTokenValid = 50;
            const int runInterval = 30;
            const int runAfterError = 2;
            
            
            long diffDate = DateTime.UtcNow.AddMinutes(runInterval-safeTokenValid).Ticks;
            long diffDateAfterError = DateTime.UtcNow.AddMinutes(-runAfterError).Ticks;

            UserDocument result = await
                _mongoCollectionProvider.UserDocumentCollection()
                    .Find(x => 
                        (!x.SetTokenDateTime.HasValue 
                         || (x.SetTokenDateTime.Value) <= diffDate) 
                        || 
                        (x.RefreshTokenError && x.SetTokenDateTime.Value<=diffDateAfterError)).FirstOrDefaultAsync();
                    
            return _objectToDocumentMapper.Convert(result);
        }
    }
}