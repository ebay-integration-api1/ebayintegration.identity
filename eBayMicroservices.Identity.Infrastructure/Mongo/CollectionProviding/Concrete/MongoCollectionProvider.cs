﻿using eBayMicroservices.Identity.Infrastructure.Mongo.CollectionProviding.Abstract;
using eBayMicroservices.Identity.Infrastructure.Mongo.Documents;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace eBayMicroservices.Identity.Infrastructure.Mongo.CollectionProviding.Concrete
{
    public class MongoCollectionProvider : IMongoCollectionProvider
    {
        private readonly IMongoClient _client;
        private readonly string _db;

        public MongoCollectionProvider(IConfiguration configuration)
        {
            string connString = configuration.GetSection("mongo").GetSection("connectionString").Value;
            _db = configuration.GetSection("mongo").GetSection("database").Value;
            _client = new MongoClient(connString);
        }


        public IMongoCollection<UserDocument> UserDocumentCollection()
        {
            return _client.GetDatabase(_db).GetCollection<UserDocument>("Users");
        }

        public IMongoCollection<MarketplaceDocument> MarketplaceDocumentCollection()
        {
            return _client.GetDatabase(_db).GetCollection<MarketplaceDocument>("Marketplaces");
        }
    }
}