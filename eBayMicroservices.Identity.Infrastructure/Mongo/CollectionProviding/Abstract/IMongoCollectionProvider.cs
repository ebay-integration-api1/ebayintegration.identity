﻿using eBayMicroservices.Identity.Infrastructure.Mongo.Documents;
using MongoDB.Driver;

namespace eBayMicroservices.Identity.Infrastructure.Mongo.CollectionProviding.Abstract
{
    public interface IMongoCollectionProvider
    {
        IMongoCollection<UserDocument> UserDocumentCollection();
        IMongoCollection<MarketplaceDocument> MarketplaceDocumentCollection();
    }
}