﻿using eBayMicroservices.Identity.Core.Entities;

namespace eBayMicroservices.Identity.Infrastructure.Mongo.Documents.Abstract
{
    public interface IObjectDocumentMapper
    {
        User Convert(UserDocument document);
        UserDocument Convert(User user);
    }
}