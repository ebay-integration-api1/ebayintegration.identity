﻿using System;
using System.Globalization;
using eBayMicroservices.Identity.Core.Entities;
using eBayMicroservices.Identity.Infrastructure.Mongo.Documents.Abstract;

namespace eBayMicroservices.Identity.Infrastructure.Mongo.Documents.Concrete
{
    public class ObjectDocumentMapper : IObjectDocumentMapper
    {
        public User Convert(UserDocument document)
        {
            return document is null ? null : new User(document.Id,document.Name,document.Login,document.Password,document.AuthenticationCode,document.AccessToken,document.RefreshToken,document.Marketplace,document.SetTokenDateTime,document.IsTestUser);
        }
        public UserDocument Convert(User user)
        {
            return user is null
                ? null
                : new UserDocument
                {
                    Id = user.Id.Value,
                    Login = user.Login,
                    Name = user.Name,
                    Password = user.Password,
                    Marketplace = user.Marketplace,
                    AccessToken = user.AccessToken,
                    AuthenticationCode = user.AuthenticationCode,
                    RefreshToken = user.RefreshToken,
                    SetTokenDateTime = user.SetTokenDateTime,
                    SetTokenDateTimeString = user.SetTokenDateTime.HasValue ? new DateTime(user.SetTokenDateTime.Value).ToString(CultureInfo.InvariantCulture) : null,
                    IsTestUser = user.IsTestUser
                };
        }
    }
}