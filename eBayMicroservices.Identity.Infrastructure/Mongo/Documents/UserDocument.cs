﻿using System;
using Convey.Types;

namespace eBayMicroservices.Identity.Infrastructure.Mongo.Documents
{
    public class UserDocument : IIdentifiable<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string AuthenticationCode { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public long? SetTokenDateTime { get; set; }
        public string SetTokenDateTimeString { get; set; }
        public int? ExpiresIn { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool RefreshTokenError { get; set; }
        public string Marketplace { get; set; }
        public bool IsTestUser { get; set; }
    }
}