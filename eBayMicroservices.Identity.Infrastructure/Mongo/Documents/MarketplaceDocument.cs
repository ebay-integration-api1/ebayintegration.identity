﻿using Convey.Types;

namespace eBayMicroservices.Identity.Infrastructure.Mongo.Documents
{
    public class MarketplaceDocument : IIdentifiable<string>
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Id => Code;
    }
}