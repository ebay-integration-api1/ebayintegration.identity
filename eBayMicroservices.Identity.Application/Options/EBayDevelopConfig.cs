﻿namespace eBayMicroservices.Identity.Application.Options
{
    public class EBayDevelopConfig
    {
        public EBayDevelopKeySet Prod { get; set; }
        public EBayDevelopKeySet Sandbox { get; set; }
    }
}