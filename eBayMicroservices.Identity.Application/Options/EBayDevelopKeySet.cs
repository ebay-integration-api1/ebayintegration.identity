﻿namespace eBayMicroservices.Identity.Application.Options
{
    public class EBayDevelopKeySet
    {
        public string DevId { get; set; }
        public string AppId { get; set; }
        public string CertId { get; set; }
        public string RedirectUrl { get; set; }
    }
}