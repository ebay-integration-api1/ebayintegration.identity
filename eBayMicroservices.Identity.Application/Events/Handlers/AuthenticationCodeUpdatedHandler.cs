﻿using System.Threading.Tasks;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Core.Events.Concrete;

namespace eBayMicroservices.Identity.Application.Events.Handlers
{
    public class AuthenticationCodeUpdatedHandler : IDomainEventHandler<AuthenticationCodeUpdated>
    {
        private readonly ITokenRefreshExecutor _tokenRefreshExecutor;

        public AuthenticationCodeUpdatedHandler(ITokenRefreshExecutor tokenRefreshExecutor)
        {
            _tokenRefreshExecutor = tokenRefreshExecutor;
        }

        public async Task HandleAsync(AuthenticationCodeUpdated @event)
        {
            await _tokenRefreshExecutor.ExecuteTokenRefresh(@event.User);
        }
    }
}