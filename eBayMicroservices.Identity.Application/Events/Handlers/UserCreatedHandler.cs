﻿using System.Threading.Tasks;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Core.Events.Concrete;

namespace eBayMicroservices.Identity.Application.Events.Handlers
{
    public class UserCreatedHandler : IDomainEventHandler<UserCreated>
    {
        private readonly ITokenRefreshExecutor _tokenRefreshExecutor;

        public UserCreatedHandler(ITokenRefreshExecutor tokenRefreshExecutor)
        {
            _tokenRefreshExecutor = tokenRefreshExecutor;
        }

        public async Task HandleAsync(UserCreated @event)
        {
            await _tokenRefreshExecutor.ExecuteTokenRefresh(@event.User);
        }
    }
}