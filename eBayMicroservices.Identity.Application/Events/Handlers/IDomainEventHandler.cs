﻿using System.Threading.Tasks;
using eBayMicroservices.Identity.Core.Events.Abstract;

namespace eBayMicroservices.Identity.Application.Events.Handlers
{
    public interface IDomainEventHandler<in T> where T : class, IDomainEvent
    {
        Task HandleAsync(T @event);
    }
}