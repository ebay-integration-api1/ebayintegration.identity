﻿using Convey.CQRS.Commands;

namespace eBayMicroservices.Identity.Application.Commands
{
    public class SignIn : ICommand
    {
        public string Login { get; }
        public string Password { get; }

        public SignIn(string login, string password)
        {
            Login = login;
            Password = password;
        }
    }
}