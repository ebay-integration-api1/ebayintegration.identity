﻿using System;
using Convey.CQRS.Commands;

namespace eBayMicroservices.Identity.Application.Commands
{
    public class UpdateUserAuthCode : ICommand
    {
        public Guid UserId { get; }
        public string AuthorisationCode { get; }

        public UpdateUserAuthCode(Guid userId, string authorisationCode)
        {
            UserId = userId;
            AuthorisationCode = authorisationCode;
        }
    }
}