﻿using System.Threading.Tasks;
using Convey.CQRS.Commands;
using eBayMicroservices.Identity.Application.Services;

namespace eBayMicroservices.Identity.Application.Commands.Handlers
{
    public class RefreshUserAccessTokensHandler : ICommandHandler<RefreshUserAccessTokens>
    {
        private readonly ITokenRefreshExecutor _tokenRefreshExecutor;

        public RefreshUserAccessTokensHandler(ITokenRefreshExecutor tokenRefreshExecutor)
        {
            _tokenRefreshExecutor = tokenRefreshExecutor;
        }
        public async Task HandleAsync(RefreshUserAccessTokens command)
        {
            await _tokenRefreshExecutor.ExecuteTokenRefresh();
        }
    }
}