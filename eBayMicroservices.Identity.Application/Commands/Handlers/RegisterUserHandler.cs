﻿using System.Threading.Tasks;
using Convey.CQRS.Commands;
using eBayMicroservices.Identity.Application.Exceptions.Concrete;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Core.Entities;
using eBayMicroservices.Identity.Core.Repositories;

namespace eBayMicroservices.Identity.Application.Commands.Handlers
{
    public class RegisterUserHandler : ICommandHandler<RegisterUser>
    {
        private readonly IUserRepository _userRepository;
        private readonly IEventProcessor _eventProcessor;
        private readonly IMarketplaceRepository _marketplaceRepository;

        public RegisterUserHandler(IUserRepository userRepository, IEventProcessor eventProcessor, IMarketplaceRepository marketplaceRepository)
        {
            _userRepository = userRepository;
            _eventProcessor = eventProcessor;
            _marketplaceRepository = marketplaceRepository;
        }

        public async Task HandleAsync(RegisterUser command)
        {
            if (await _userRepository.ExistsAsync(command.Login))
            {
                throw new UserAlreadyExists(command.Login);
            }

            if (!await _marketplaceRepository.ExistsAsync(command.Marketplace))
            {
                throw new NotExistingMarketplaceWhileSigningUpUser(command.Marketplace);
            }
            
            User user = User.Create(command.Id, command.Name, command.Login, command.Password, command.AuthenticationCode,command.Marketplace,command.IsTestUser);

            await _userRepository.AddAsync(user);

            await _eventProcessor.ProcessAsync(user.Events);
        }
    }
}