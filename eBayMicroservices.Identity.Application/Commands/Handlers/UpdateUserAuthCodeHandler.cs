﻿using System.Threading.Tasks;
using Convey.CQRS.Commands;
using eBayMicroservices.Identity.Application.Exceptions.Concrete;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Core.Entities;
using eBayMicroservices.Identity.Core.Repositories;

namespace eBayMicroservices.Identity.Application.Commands.Handlers
{
    public class UpdateUserAuthCodeHandler : ICommandHandler<UpdateUserAuthCode>
    {
        private readonly IUserRepository _userRepository;
        private readonly IEventProcessor _eventProcessor;

        public UpdateUserAuthCodeHandler(IUserRepository userRepository, IEventProcessor eventProcessor)
        {
            _userRepository = userRepository;
            _eventProcessor = eventProcessor;
        }

        public async Task HandleAsync(UpdateUserAuthCode command)
        {
            User user = await _userRepository.GetUserById(command.UserId);
            if (user is null)
            {
                throw new UserNotFoundException(command.UserId.ToString());
            }

            user.UpdateAuthenticationCode(command.AuthorisationCode);

            await _userRepository.UpdateUserAuthenticationCode(command.UserId, command.AuthorisationCode);

            await _eventProcessor.ProcessAsync(user.Events);
        }
    }
}