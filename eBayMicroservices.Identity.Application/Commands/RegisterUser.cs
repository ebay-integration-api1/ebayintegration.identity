﻿using System;
using Convey.CQRS.Commands;

namespace eBayMicroservices.Identity.Application.Commands
{
    public class RegisterUser : ICommand
    {
        public Guid Id { get; }
        public string Login { get; }
        public string Name { get; }
        public string Password { get; }
        public string AuthenticationCode { get; }
        public string Marketplace { get; }
        public bool IsTestUser { get; }

        public RegisterUser(Guid id, string login, string name, string password,
            string marketplace,string authenticationCode, bool isTestUser)
        {
            Id = id;
            Login = login;
            Name = name;
            Password = password;
            Marketplace = marketplace;
            AuthenticationCode = authenticationCode;
            IsTestUser = isTestUser;
        }
    }
}