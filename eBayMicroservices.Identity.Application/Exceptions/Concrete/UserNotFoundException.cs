﻿using eBayMicroservices.Identity.Application.Exceptions.Abstract;

namespace eBayMicroservices.Identity.Application.Exceptions.Concrete
{
    public class UserNotFoundException : AppException
    {
        public string Identifier { get; }

        public UserNotFoundException(string identifier) : base($"User not found by identifier: {identifier}")
        {
            Identifier = identifier;
        }

        public override string Code => "user_not_found";
    }
}