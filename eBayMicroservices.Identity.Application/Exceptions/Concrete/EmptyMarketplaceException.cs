﻿using eBayMicroservices.Identity.Application.Exceptions.Abstract;

namespace eBayMicroservices.Identity.Application.Exceptions.Concrete
{
    public class EmptyMarketplaceException : AppException
    {
        public EmptyMarketplaceException() : base("given empty marketplace parameter")
        {
        }

        public override string Code => "marketplace_is_empty";
    }
}