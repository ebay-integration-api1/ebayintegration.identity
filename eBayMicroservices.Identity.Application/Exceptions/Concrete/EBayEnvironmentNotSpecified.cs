﻿using eBayMicroservices.Identity.Application.Exceptions.Abstract;

namespace eBayMicroservices.Identity.Application.Exceptions.Concrete
{
    public class EBayEnvironmentNotSpecified : AppException
    {
        public EBayEnvironmentNotSpecified() : base("Please set variable sandbox : true for test environment, false for production environment")
        {
        }

        public override string Code => "eBay_environment_not_specified";
    }
}