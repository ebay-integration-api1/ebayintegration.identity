﻿using eBayMicroservices.Identity.Application.Exceptions.Abstract;

namespace eBayMicroservices.Identity.Application.Exceptions.Concrete
{
    public class UnauthorisedUserException : AppException
    {
        public override string Code => "unauthorised_user";

        public UnauthorisedUserException() : base("Unauthorised user in secured method")
        {
            
        }

    }
}