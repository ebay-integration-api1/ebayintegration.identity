﻿using eBayMicroservices.Identity.Application.Exceptions.Abstract;

namespace eBayMicroservices.Identity.Application.Exceptions.Concrete
{
    public class EBayApiException : AppException
    {
        
        public EBayApiException(int? statusCode) : base($"EBay api exception. StatusCode: {statusCode}. Please contact with administrator.")
        {
        }

        public override string Code => "eBay_api_exception";
    }
}