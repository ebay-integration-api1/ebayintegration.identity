﻿using eBayMicroservices.Identity.Application.Exceptions.Abstract;

namespace eBayMicroservices.Identity.Application.Exceptions.Concrete
{
    public class UserAlreadyExists : AppException
    {
        public string Login { get; }

        public UserAlreadyExists(string login) : base($"user with login {login} already exists")
        {
            Login = login;
        }

        public override string Code => "user_already_exists";
    }
}