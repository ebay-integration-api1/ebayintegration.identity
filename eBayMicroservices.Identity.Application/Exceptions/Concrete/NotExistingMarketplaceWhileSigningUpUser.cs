﻿using eBayMicroservices.Identity.Application.Exceptions.Abstract;

namespace eBayMicroservices.Identity.Application.Exceptions.Concrete
{
    public class NotExistingMarketplaceWhileSigningUpUser : AppException
    {
        public string Marketplace { get; }

        public NotExistingMarketplaceWhileSigningUpUser(string marketplace) : base("Invalid marketplace code while creating ")
        {
            Marketplace = marketplace;
        }

        public override string Code => "invalid_marketplace_code_while_signing_up_user";
    }
}