﻿using System;

namespace eBayMicroservices.Identity.Application.Exceptions.Abstract
{
    public abstract class AppException : Exception
    {
        public abstract string Code { get; }

        protected AppException(string message) : base(message)
        {
        }
    }
}