﻿namespace eBayMicroservices.Identity.Application.DTOs.Policies
{
    public abstract class PolicyBaseDto
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
    }
}