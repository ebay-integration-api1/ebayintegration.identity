﻿using System.ComponentModel.DataAnnotations;

namespace eBayMicroservices.Identity.Application.DTOs
{
    
    public class UserDtoToExternalUse
    {
        /// <summary>
        /// User Name
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// User Login
        /// </summary>
        [Required]
        public string Login { get; set; }
        
        /// <summary>
        /// List of marketplaces
        /// </summary>
        [Required]
        public string Marketplace { get; set; }
        /// <summary>
        /// User exists only in Sandbox
        /// </summary>
        [Required]
        public bool IsTestUser { get; set; }
    }
}