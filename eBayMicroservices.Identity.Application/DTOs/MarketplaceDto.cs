﻿namespace eBayMicroservices.Identity.Application.DTOs
{
    public class MarketplaceDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}