﻿using System;

namespace eBayMicroservices.Identity.Application.DTOs
{
    /// <summary>
    /// UserDto
    /// </summary>
    public class UserDto : UserDtoToExternalUse
    {
        /// <summary>
        /// User Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Client access token
        /// </summary>
        public string AccessToken { get; set; }
    }
}