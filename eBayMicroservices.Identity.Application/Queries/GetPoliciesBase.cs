﻿using System;

namespace eBayMicroservices.Identity.Application.Queries
{
    public class GetPoliciesBase
    {
        public string Marketplace { get; set; }
        public Guid UserId { get; set; }
    }
}