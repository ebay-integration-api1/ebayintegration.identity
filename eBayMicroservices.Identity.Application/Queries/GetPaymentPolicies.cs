﻿using System.Collections.Generic;
using Convey.CQRS.Queries;
using eBayMicroservices.Identity.Application.DTOs.Policies;

namespace eBayMicroservices.Identity.Application.Queries
{
    public class GetPaymentPolicies : GetPoliciesBase,IQuery<IEnumerable<PaymentPolicyDto>>
    {
    }
}