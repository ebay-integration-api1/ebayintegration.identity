﻿using Convey.CQRS.Queries;

namespace eBayMicroservices.Identity.Application.Queries
{
    public class GetUrlToSignIn : IQuery<string>
    {
        public bool? IsSandbox { get; set; }
    }
}