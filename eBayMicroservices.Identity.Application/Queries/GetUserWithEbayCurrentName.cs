﻿using System;
using Convey.CQRS.Queries;
using eBayMicroservices.Identity.Application.DTOs;

namespace eBayMicroservices.Identity.Application.Queries
{
    public class GetUserWithEbayCurrentName : IQuery<UserDto>
    {
        public Guid Id { get; set; }
    }
}