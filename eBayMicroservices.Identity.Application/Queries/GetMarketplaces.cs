﻿using System.Collections.Generic;
using Convey.CQRS.Queries;
using eBayMicroservices.Identity.Application.DTOs;

namespace eBayMicroservices.Identity.Application.Queries
{
    public class GetMarketplaces : IQuery<IEnumerable<MarketplaceDto>>
    {
        
    }
}