﻿using System;
using eBayMicroservices.Identity.Application.Services;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace eBayMicroservices.Identity.Application.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class OnlyLoggedInUser : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            IAuthenticator authenticator = context.HttpContext.RequestServices.GetService<IAuthenticator>();
            
            authenticator.Authenticate(context.HttpContext);
            
            base.OnActionExecuting(context);
        }
    }
}