﻿using System.Threading.Tasks;

namespace eBayMicroservices.Identity.Application.Services
{
    public interface IEBayIdentityServiceClient
    {
        Task<string> GetUserName(string userAccessToken);
    }
}