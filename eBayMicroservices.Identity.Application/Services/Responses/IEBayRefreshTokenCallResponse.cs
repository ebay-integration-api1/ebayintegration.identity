﻿namespace eBayMicroservices.Identity.Application.Services.Responses
{
    public interface IEBayRefreshTokenCallResponse
    {
        bool Success { get; }
        string AccessToken { get; }
        string ExpiresIn { get; }
        string RefreshToken { get; }
    }
}