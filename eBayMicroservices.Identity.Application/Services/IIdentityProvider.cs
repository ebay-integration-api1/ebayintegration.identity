﻿using System;
using Microsoft.AspNetCore.Http;

namespace eBayMicroservices.Identity.Application.Services
{
    public interface IIdentityProvider
    {
        Guid GetIdentity(HttpContext ctx);
        string GetIdentityAsString(HttpContext ctx);
    }
}