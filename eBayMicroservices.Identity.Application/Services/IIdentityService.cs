﻿using System;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Application.Commands;

namespace eBayMicroservices.Identity.Application.Services
{
    public interface IIdentityService
    {
        Task<string> SignInAsync(SignIn command);
        Guid? ValidateUserByToken(string token);
    }
}