﻿namespace eBayMicroservices.Identity.Application.Services
{
    public interface IBaseEncodeConverter
    {
        string Encode(string toConvert);
        string Decode(string toConvert);
    }
}