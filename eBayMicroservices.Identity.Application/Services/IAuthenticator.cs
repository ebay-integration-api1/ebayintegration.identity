﻿using Microsoft.AspNetCore.Http;

namespace eBayMicroservices.Identity.Application.Services
{
    public interface IAuthenticator
    {
        public void Authenticate(HttpContext ctx);
    }
}