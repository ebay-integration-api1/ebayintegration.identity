﻿using System.Threading.Tasks;
using eBayMicroservices.Identity.Application.Services.Responses;

namespace eBayMicroservices.Identity.Application.Services
{
    public interface IEBayRefreshTokenCallExecutor
    {
        Task<IEBayRefreshTokenCallResponse> CreateFirstTokens(string code,bool isTestUser);
        Task<IEBayRefreshTokenCallResponse> RefreshTokens(string refreshToken,bool isTestUser);
    }
}