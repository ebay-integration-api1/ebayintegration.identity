﻿using System.Threading.Tasks;
using eBayMicroservices.Identity.Core.Entities;

namespace eBayMicroservices.Identity.Application.Services
{
    public interface ITokenRefreshExecutor
    {
        Task ExecuteTokenRefresh();
        Task ExecuteTokenRefresh(User user);
    }
}