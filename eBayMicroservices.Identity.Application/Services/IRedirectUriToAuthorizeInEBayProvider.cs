﻿namespace eBayMicroservices.Identity.Application.Services
{
    public interface IRedirectUriToAuthorizeInEBayProvider
    {
        string UrlToEBayAuthorize(bool sandbox);
    }
}