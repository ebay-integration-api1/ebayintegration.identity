﻿using System.Collections.Generic;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Application.DTOs.Policies;

namespace eBayMicroservices.Identity.Application.Services
{
    public interface IEBayPoliciesServiceClient
    {
        Task<IEnumerable<FulfillmentPolicyDto>> GetFulfillmentPolicies(string marketplace,string accessToken,bool isTest);
        Task<IEnumerable<PaymentPolicyDto>> GetPaymentPolicies(string marketplace,string accessToken,bool isTest);
        Task<IEnumerable<ReturnPolicyDto>> GetReturnPolicies(string marketplace,string accessToken,bool isTest);
        
    }
}