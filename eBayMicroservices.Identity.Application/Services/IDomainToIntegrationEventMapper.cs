﻿using System.Collections.Generic;
using Convey.CQRS.Events;
using eBayMicroservices.Identity.Core.Events.Abstract;

namespace eBayMicroservices.Identity.Application.Services
{
    public interface IDomainToIntegrationEventMapper
    {
        IEnumerable<IEvent> MapAll(IEnumerable<IDomainEvent> events);
        IEvent Map(IDomainEvent @event);
        
    }
}