﻿using System.Collections.Generic;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Core.Events.Abstract;

namespace eBayMicroservices.Identity.Application.Services
{
    public interface IEventProcessor
    {
        Task ProcessAsync(IEnumerable<IDomainEvent> events);
    }
}