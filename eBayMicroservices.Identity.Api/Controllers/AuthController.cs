﻿using System;
using System.Threading.Tasks;
using System.Web;
using Convey.CQRS.Commands;
using Convey.CQRS.Queries;
using eBayMicroservices.Identity.Api.Mapping;
using eBayMicroservices.Identity.Api.Models;
using eBayMicroservices.Identity.Application.Attributes;
using eBayMicroservices.Identity.Application.Commands;
using eBayMicroservices.Identity.Application.DTOs;
using eBayMicroservices.Identity.Application.Queries;
using eBayMicroservices.Identity.Application.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace eBayMicroservices.Identity.Api.Controllers
{
    [ApiController]
    [Route("api/identity")]
    public class AuthController : ControllerBase
    {
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly IQueryDispatcher _queryDispatcher;
        private readonly IIdentityService _identityService;
        private readonly IIdentityProvider _identityProvider;

        public AuthController(ICommandDispatcher commandDispatcher, IQueryDispatcher queryDispatcher,
            IIdentityService identityService, IIdentityProvider identityProvider)
        {
            _commandDispatcher = commandDispatcher;
            _queryDispatcher = queryDispatcher;
            _identityService = identityService;
            _identityProvider = identityProvider;
        }

        /// <summary>
        /// Method to sign new user with their credentials
        /// </summary>
        /// <param name="model">Request contains information about new user</param>
        [AllowAnonymous]
        [ProducesResponseType(typeof(object), 201)]
        [ProducesResponseType(typeof(object), 400)]
        [HttpPost("sign-up")]
        public async Task<ActionResult> Register([FromBody] RegisterUserRequestModel model)
        {
            Guid id = Guid.NewGuid();

            RegisterUser registerUser = new RegisterUser(id, model.Login, model.Name,
                model.Password, model.Marketplace, HttpUtility.UrlDecode(model.AuthenticationCode), model.IsTestUser);

            await _commandDispatcher.SendAsync(registerUser);

            return Created($"api/user/{id}", null);
        }

        /// <summary>
        /// Method to sign in user
        /// </summary>
        /// <param name="model">Request contains information about new user</param>
        /// <returns>Authorization token</returns>
        [AllowAnonymous]
        [HttpPost("sign-in")]
        [ProducesResponseType(typeof(SignInResultModel), 200)]
        [ProducesResponseType(typeof(object), 400)]
        [Produces("application/json")]
        public async Task<ActionResult> Login([FromBody] LoginUserRequestModel model)
        {
            SignIn signIn = new SignIn(model.Login, model.Password);

            string token = await _identityService.SignInAsync(signIn);

            return Ok(new SignInResultModel
            {
                Token = token
            });
        }

        //[NonAction]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpPost("check")]
        public ActionResult Check([FromBody] ValidateUserRequest req)
        {
            Guid? currentlyLoggedIn = _identityService.ValidateUserByToken(req.Token);

            return Ok(currentlyLoggedIn ?? Guid.Empty);
        }

        /// <summary>
        /// Method available only inside k8s, will be invisible for Release
        /// Method to get User credentials by Header identifier
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ApiExplorerSettings(IgnoreApi = true)]
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(typeof(object), 404)]
        [HttpGet("{id:Guid}")]
        public async Task<ActionResult> GetUser(Guid id)
        {
            UserDto result = await GetUserById(id);
            if (result is null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        /// <summary>
        /// Method to refresh eBay credentials
        /// </summary>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(typeof(object), 404)]
        [HttpPut("auth-code")]
        [OnlyLoggedInUser]
        public async Task<ActionResult> UpdateAuthCode([FromBody] UpdateAuthCodeRequestModel requestModel)
        {
            Guid id = _identityProvider.GetIdentity(HttpContext);
            UpdateUserAuthCode command = new UpdateUserAuthCode(id, requestModel.Code);

            await _commandDispatcher.SendAsync(command);
            return NoContent();
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(typeof(object), 404)]
        [HttpPut("tokens")]
        public async Task<ActionResult> TriggerAuthTokenRefresh()
        {
            RefreshUserAccessTokens command = new RefreshUserAccessTokens();
            await _commandDispatcher.SendAsync(command);
            return NoContent();
        }

        /// <summary>
        /// Method returns current logged id user
        /// </summary>
        /// <returns></returns>
        [HttpGet("me")]
        [OnlyLoggedInUser]
        [ProducesResponseType(typeof(UserDtoToExternalUse), 200)]
        [ProducesResponseType(typeof(object), 401)]
        public async Task<ActionResult> GetCurrentUser()
        {
            UserDto userDto = await _queryDispatcher.QueryAsync(
                new GetUserWithEbayCurrentName
                {
                    Id = _identityProvider.GetIdentity(HttpContext)
                });

            return Ok(userDto.AsExternalDto());
        }


        private async Task<UserDto> GetUserById(Guid id)
        {
            GetUser query = new GetUser
            {
                Id = id
            };
            UserDto userDto = await _queryDispatcher.QueryAsync(query);

            return userDto;
        }

        /// <summary>
        /// Method to get global credentials to eBay MWS
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(typeof(GetUrlToSignInResponse), 200)]
        [ProducesResponseType(typeof(object), 400)]
        [AllowAnonymous]
        [HttpGet("getUrlToSignIn")]
        public async Task<ActionResult> GetAuthorizeRedirectUri(bool? sandbox)
        {
            GetUrlToSignIn query = new GetUrlToSignIn
            {
                IsSandbox = sandbox
            };

            return Ok(new GetUrlToSignInResponse
            {
                RedirectUrl = await _queryDispatcher.QueryAsync(query)
            });
        }
    }
}