﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Convey.CQRS.Queries;
using eBayMicroservices.Identity.Application.Attributes;
using eBayMicroservices.Identity.Application.DTOs.Policies;
using eBayMicroservices.Identity.Application.Queries;
using eBayMicroservices.Identity.Application.Services;
using Microsoft.AspNetCore.Mvc;

namespace eBayMicroservices.Identity.Api.Controllers
{
    [ApiController]
    [Route("api/policies")]
    public class PoliciesController : ControllerBase
    {
        private readonly IQueryDispatcher _queryDispatcher;
        private readonly IIdentityProvider _identityProvider;
        public PoliciesController(IQueryDispatcher queryDispatcher, IIdentityProvider identityProvider)
        {
            _queryDispatcher = queryDispatcher;
            _identityProvider = identityProvider;
        }

        [HttpGet("fulfillment")]
        [OnlyLoggedInUser]
        public async Task<ActionResult> FulfillmentPolicies(string marketplace)
        {
            GetFulfillmentPolicies query = new GetFulfillmentPolicies
            {
                Marketplace = marketplace,
                UserId = _identityProvider.GetIdentity(HttpContext)
            };
            
            IEnumerable<FulfillmentPolicyDto> policies = await _queryDispatcher.QueryAsync(query);
            
            return Ok(policies);
            
        }
        
        [HttpGet("payment")]
        [OnlyLoggedInUser]
        public async Task<ActionResult> PaymentPolicies(string marketplace)
        {
            GetPaymentPolicies query = new GetPaymentPolicies
            {
                Marketplace = marketplace,
                UserId = _identityProvider.GetIdentity(HttpContext)
            };
            
            IEnumerable<PaymentPolicyDto> policies = await _queryDispatcher.QueryAsync(query);
            
            return Ok(policies);
        }
        
        [HttpGet("return")]
        [OnlyLoggedInUser]
        public async Task<ActionResult> ReturnPolicies(string marketplace)
        {
             GetReturnPolicies query = new GetReturnPolicies
             {
                 Marketplace = marketplace,
                 UserId = _identityProvider.GetIdentity(HttpContext)
             };
            
             IEnumerable<ReturnPolicyDto> policies = await _queryDispatcher.QueryAsync(query);

            return Ok(policies);
        }
    }
}