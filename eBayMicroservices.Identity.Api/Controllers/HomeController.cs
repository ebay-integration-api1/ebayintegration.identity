﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace eBayMicroservices.Identity.Api.Controllers
{
    [ApiController]
    [Route("api")]
    public class HomeController : ControllerBase
    {
        private static readonly Guid InstanceId = Guid.NewGuid();
        
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet("/ping")]
        public IActionResult HealthCheck()
        {
            return Ok($"Hello from identity Service ({InstanceId})");
        }
    }
}