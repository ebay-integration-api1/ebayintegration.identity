﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Convey.CQRS.Queries;
using eBayMicroservices.Identity.Application.DTOs;
using eBayMicroservices.Identity.Application.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace eBayMicroservices.Identity.Api.Controllers
{
    [ApiController]
    [Route("api/marketplaces")]
    public class MarketplaceController : ControllerBase
    {
        private readonly IQueryDispatcher _queryDispatcher;

        public MarketplaceController(IQueryDispatcher queryDispatcher)
        {
            _queryDispatcher = queryDispatcher;
        }
        /// <summary>
        /// Method to get available Marketplaces
        /// </summary>
        [AllowAnonymous]
        [ProducesResponseType(typeof(IEnumerable<MarketplaceDto>), 200)]
        [HttpGet]
        public async Task<ActionResult> GetMarketplaces()
            => Ok(await _queryDispatcher.QueryAsync(new GetMarketplaces()));
    }
}