﻿
 using eBayMicroservices.Identity.Application.DTOs;

 namespace eBayMicroservices.Identity.Api.Mapping
{
    public static class DtoExtension
    {
        public static UserDtoToExternalUse AsExternalDto(this UserDto userDto)
            => new UserDtoToExternalUse
            {
                Login = userDto.Login,
                Name = userDto.Name,
                Marketplace = userDto.Marketplace,
                IsTestUser = userDto.IsTestUser
            };
    }
}