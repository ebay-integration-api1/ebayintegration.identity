﻿namespace eBayMicroservices.Identity.Api.Models
{
    public class UpdateAuthCodeRequestModel
    {
        public string Code { get; set; }
    }
}