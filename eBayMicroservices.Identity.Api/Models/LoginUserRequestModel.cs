﻿using System.ComponentModel.DataAnnotations;

namespace eBayMicroservices.Identity.Api.Models
{
    public class LoginUserRequestModel
    {
        /// <summary>
        /// User login
        /// </summary>
        [Required]
        public string Login { get; set; }
        /// <summary>
        /// User password
        /// </summary>
        [Required]
        public string Password { get; set; }
    }
}