﻿namespace eBayMicroservices.Identity.Api.Models
{
    public class ValidateUserRequest
    {
        public string Token { get; set; }
    }
}