﻿namespace eBayMicroservices.Identity.Api.Models
{
    public class SetGlobalKeyRequest
    {
        /// <summary>
        /// Comarch Developer key
        /// </summary>
        public string DevKey { get; set; }
        /// <summary>
        /// Comarch Aws key
        /// </summary>
        public string ClientId { get; set; }
        /// <summary>
        /// Comarch Developer secret
        /// </summary>
        public string ClientHash { get; set; }
        /// <summary>
        /// Comarch eBay Developer Sandbox RuName
        /// </summary>
        public string EBaySandboxRedirectPageName { get; set; }
        /// <summary>
        /// Comarch eBay Developer RuName
        /// </summary>
        public string EBayRedirectPageName { get; set; }
    }
}