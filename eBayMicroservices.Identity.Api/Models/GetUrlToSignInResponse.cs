﻿namespace eBayMicroservices.Identity.Api.Models
{
    /// <summary>
    /// Object contains redirectURL to sign in in eBay
    /// </summary>
    public class GetUrlToSignInResponse
    {
        /// <summary>
        /// Url to sign in in eBay
        /// </summary>
        public string RedirectUrl { get; set; }
    }
}