﻿using System.ComponentModel.DataAnnotations;

namespace eBayMicroservices.Identity.Api.Models
{
    public class RegisterUserRequestModel
    {
        /// <summary>
        /// Login field to sign in
        /// </summary>
        [Required]
        public string Login { get; set; }
        /// <summary>
        /// Name of new user
        /// </summary>
        [Required]
        public string Name { get; set; }
        
        /// <summary>
        /// Password to verify user
        /// </summary>
        [Required]
        public string Password { get; set; }
        /// <summary>
        /// Marketplace Code
        /// </summary>
        [Required]
        public string Marketplace { get; set; }
        /// <summary>
        /// Code to generate 
        /// </summary>
        public string AuthenticationCode { get; set; }
        /// <summary>
        /// Flag means is user for test in sandbox
        /// </summary>
        public bool IsTestUser { get; set; }
    }
}