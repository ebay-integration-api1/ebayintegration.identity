﻿using System.ComponentModel.DataAnnotations;

namespace eBayMicroservices.Identity.Api.Models
{
    public class SignInResultModel
    {
        /// <summary>
        /// Token to set in Header by key "Authorization"
        /// </summary>
        [Required]
        public string Token { get; set; }
    }
}