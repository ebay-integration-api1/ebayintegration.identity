﻿using System;
using eBayMicroservices.Identity.Core.Entities;
using eBayMicroservices.Identity.Core.Exceptions;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Core.Entities
{
    public class AggregateIdTests
    {
        public static AggregateId Act(Guid id) => new AggregateId(id);
        public static AggregateId ActWithoutId() => new AggregateId();

        [Fact]
        public void given_valid_id_should_be_succeed()
        {
            Guid id = Guid.NewGuid();

            AggregateId result = Act(id);
            result.ShouldNotBeNull();
            result.Value.ShouldBe(id);
        }
        [Fact]
        public void given_invalid_id_should_throw_an_exception()
        {
            Guid id = Guid.Empty;

            Exception ex = Record.Exception(() => Act(id));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<InvalidAggregateIdException>();
            (ex as InvalidAggregateIdException)?.Id.ShouldBe(id);
        }

        [Fact]
        public void use_constructor_without_parameter_should_be_succeed()
        {
            AggregateId aggregateId = ActWithoutId();
            
            aggregateId.ShouldNotBeNull();
            aggregateId.Value.ShouldNotBe(Guid.Empty);
        }
    }
}