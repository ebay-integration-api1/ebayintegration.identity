﻿using System;
using System.Linq;
using eBayMicroservices.Identity.Core.Entities;
using eBayMicroservices.Identity.Core.Events.Concrete;
using eBayMicroservices.Identity.Core.Exceptions;
using Shouldly;
using Xunit;
// ReSharper disable PossibleNullReferenceException

namespace eBayMicroservices.Identity.Tests.Unit.Core.Entities
{
    public class UserTests
    {
        private static User Act(Guid id,string name, string login, string password, string authenticationCode,string marketplace,bool isTestUser) 
            => User.Create(id,name,login,password,authenticationCode,marketplace,isTestUser);

        [Fact]
        public void given_all_valid_data_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = "user_login_valid";
            const string password = "user_password_valid";
            const string authenticationCode = "user_auth_token";
            const string marketplace = "EBAY_PL";
            const bool isTestUser = false;

            User result = Act(id, name, login, password,authenticationCode, marketplace,isTestUser);
            
            result.ShouldNotBeNull();
            result.Id.Value.ShouldBe(id);
            result.Login.ShouldBe(login);
            result.Name.ShouldBe(name);
            result.Password.ShouldBe(password);
            result.Marketplace.ShouldBe(marketplace);
            result.AuthenticationCode.ShouldBe(authenticationCode);
            result.IsTestUser.ShouldBe(isTestUser);
            result.Events.Count().ShouldBe(1);
            result.Events.SingleOrDefault().ShouldBeOfType<UserCreated>();
            // ReSharper disable once PossibleNullReferenceException
            ((UserCreated) result.Events.SingleOrDefault()).User.Id.Value.ShouldBe(id);
        }
        
        [Fact]
        public void given_all_valid_data_with_testuser_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = "user_login_valid";
            const string password = "user_password_valid";
            const string authenticationCode = "user_auth_token";
            const string marketplace = "EBAY_PL";
            const bool isTestUser = true;

            User result = Act(id, name, login, password, authenticationCode, marketplace,isTestUser);
            
            result.ShouldNotBeNull();
            result.Id.Value.ShouldBe(id);
            result.Login.ShouldBe(login);
            result.Name.ShouldBe(name);
            result.Password.ShouldBe(password);
            result.Marketplace.ShouldBe(marketplace);
            result.AuthenticationCode.ShouldBe(authenticationCode);
            result.Events.Count().ShouldBe(1);
            result.Events.SingleOrDefault().ShouldBeOfType<UserCreated>();
            // ReSharper disable once PossibleNullReferenceException
            ((UserCreated) result.Events.SingleOrDefault()).User.Id.Value.ShouldBe(id);
        }
        [Fact]
        public void given_empty_login_should_throw_exception()
        {
            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = "";
            const string password = "user_password_valid";
            const string authenticationCode = "user_auth_token";
            const string marketplace = "EBAY_PL";
            const bool isTestUser = false;

            Exception ex = Record.Exception(() => Act(id, name, login, password,authenticationCode, marketplace,isTestUser));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<InvalidUserDataException>();
            InvalidUserDataException typed = ex as InvalidUserDataException;
            typed.Field.ShouldBe(nameof(login));
        }
        
        [Fact]
        public void given_login_as_null_should_throw_exception()
        {
            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = null;
            const string password = "user_password_valid";
            const string authenticationCode = "user_auth_token";
            const string marketplace = "EBAY_PL";
            const bool isTestUser = false;
            
            Exception ex = Record.Exception(() => Act(id, name, login, password, authenticationCode, marketplace,isTestUser));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<InvalidUserDataException>();
            InvalidUserDataException typed = ex as InvalidUserDataException;
            typed.Field.ShouldBe(nameof(login));
        }
        
        
        
        [Fact]
        public void given_empty_name_should_throw_exception()
        {
            Guid id = Guid.NewGuid();
            const string name = "";
            const string login = "user_login_valid";
            const string password = "user_password_valid";
            const string authenticationCode = "user_auth_token";
            const string marketplace = "EBAY_PL";
            const bool isTestUser = false;

            Exception ex = Record.Exception(() => Act(id, name, login, password,authenticationCode, marketplace,isTestUser));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<InvalidUserDataException>();
            InvalidUserDataException typed = ex as InvalidUserDataException;
            typed.Field.ShouldBe(nameof(name));
        }
        
        [Fact]
        public void given_name_as_null_should_throw_exception()
        {
            Guid id = Guid.NewGuid();
            const string name = null;
            const string login = "user_login_valid";
            const string password = "user_password_valid";
            const string authenticationCode = "user_auth_token";
            const string marketplace = "EBAY_PL";
            const bool isTestUser = false;

            Exception ex = Record.Exception(() => Act(id, name, login, password,authenticationCode, marketplace,isTestUser));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<InvalidUserDataException>();
            InvalidUserDataException typed = ex as InvalidUserDataException;
            typed.Field.ShouldBe(nameof(name));
        }
        
        
        [Fact]
        public void given_password_name_should_throw_exception()
        {
            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = "user_login_valid";
            const string password = "";
            const string authenticationCode = "user_auth_token";
            const string marketplace = "EBAY_PL";
            const bool isTestUser = false;

            Exception ex = Record.Exception(() => Act(id, name, login, password,authenticationCode, marketplace,isTestUser));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<InvalidUserDataException>();
            InvalidUserDataException typed = ex as InvalidUserDataException;
            typed.Field.ShouldBe(nameof(password));
        }
        
        [Fact]
        public void given_password_as_null_should_throw_exception()
        {
            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = "user_login_valid";
            const string password = null;
            const string authenticationCode = "user_auth_token";
            const string marketplace = "EBAY_PL";
            const bool isTestUser = false;

            Exception ex = Record.Exception(() => Act(id, name, login, password, authenticationCode, marketplace,isTestUser));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<InvalidUserDataException>();
            InvalidUserDataException typed = ex as InvalidUserDataException;
            typed.Field.ShouldBe(nameof(password));
        }
        
        [Fact]
        public void given_auth_code_empty_should_throw_exception()
        {
            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = "user_login_valid";
            const string password = "user_password_valid";
            const string authenticationCode = "";
            const string marketplace = "EBAY_PL";
            const bool isTestUser = false;

            Exception ex = Record.Exception(() => Act(id, name, login, password, authenticationCode, marketplace,isTestUser));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<InvalidUserDataException>();
            InvalidUserDataException typed = ex as InvalidUserDataException;
            typed.Field.ShouldBe(nameof(authenticationCode));
        }
        
        [Fact]
        public void given_authcode_as_null_should_throw_exception()
        {
            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = "user_login_valid";
            const string password = "user_password_valid";
            const string authenticationCode = null;
            
            const string marketplace = "EBAY_PL";
            const bool isTestUser = false;

            Exception ex = Record.Exception(() => Act(id, name, login, password,authenticationCode, marketplace,isTestUser));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<InvalidUserDataException>();
            InvalidUserDataException typed = ex as InvalidUserDataException;
            typed.Field.ShouldBe(nameof(authenticationCode));
        }
    }
}