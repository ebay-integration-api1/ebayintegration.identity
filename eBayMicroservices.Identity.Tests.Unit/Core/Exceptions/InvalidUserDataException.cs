﻿using eBayMicroservices.Identity.Core.Exceptions;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Core.Exceptions
{
    public class InvalidUserDataExceptionTests
    {
        private static InvalidUserDataException Act(string field) => new InvalidUserDataException(field);
        
        [Fact]
        public void given_valid_data_should_be_succeed()
        {
            const string field = "example_field";
            InvalidUserDataException result =  Act(field);
            result.ShouldNotBeNull();
            result.Field.ShouldBe(field);
            
        }
    }
}