﻿using System;
using eBayMicroservices.Identity.Core.Exceptions;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Core.Exceptions
{
    public class InvalidAggregateIdExceptionTests
    {
        private static InvalidAggregateIdException Act(Guid id) => new InvalidAggregateIdException(id);

        
        [Fact]
        public void given_valid_data_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            InvalidAggregateIdException result = Act(id);
            
            result.ShouldNotBeNull();
            result.Id.ShouldBe(id);
        }
    }
}