﻿using System;
using System.Linq;
using eBayMicroservices.Identity.Core.Entities;
using eBayMicroservices.Identity.Core.Events.Concrete;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Core.Events
{
    public class UserCreatedTests
    {
        private static UserCreated Act(User user) => new UserCreated(user);

        [Fact]
        public void given_valid_user_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = "user_login_valid";
            const string password = "user_password_valid";
            const string authenticationCode = "user_auth_token";
            const string marketplace = "EBAY_PL";
            const bool isTestUser = false;

            User user = User.Create(id, name, login, password, authenticationCode, marketplace,isTestUser);

            UserCreated userCreated = Act(user);

            userCreated.ShouldNotBeNull();
            userCreated.User.ShouldNotBeNull();

            User result = userCreated.User;

            result.Id.Value.ShouldBe(id);
            result.Login.ShouldBe(login);
            result.Name.ShouldBe(name);
            result.Password.ShouldBe(password);
            result.Marketplace.ShouldBe(marketplace);
            result.AuthenticationCode.ShouldBe(authenticationCode);
            result.Events.Count().ShouldBe(1);
            result.Events.SingleOrDefault().ShouldBeOfType<UserCreated>();
            // ReSharper disable once PossibleNullReferenceException
            ((UserCreated) result.Events.SingleOrDefault()).User.Id.Value.ShouldBe(id);
        }
    }
}