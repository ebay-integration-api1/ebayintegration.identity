﻿using System;
using System.Linq;
using eBayMicroservices.Identity.Core.Entities;
using eBayMicroservices.Identity.Core.Events.Concrete;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Core.Events
{
    public class AuthenticationCodeUpdatedTests
    {
        private static AuthenticationCodeUpdated Act(User user) => new AuthenticationCodeUpdated(user);

        [Fact]
        public void given_valid_user_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = "user_login_valid";
            const string password = "user_password_valid";
            const string authenticationCode = "user_auth_token";
            const string marketplace = "EBAY_PL";
            const bool isTestUser = false;

            User user = new User(id,name,login,password,authenticationCode,null,null,marketplace,null,isTestUser);
            
            user.UpdateAuthenticationCode(authenticationCode);
            

            AuthenticationCodeUpdated userCreated = Act(user);

            userCreated.ShouldNotBeNull();
            userCreated.User.ShouldNotBeNull();

            User result = userCreated.User;

            result.Id.Value.ShouldBe(id);
            result.Login.ShouldBe(login);
            result.Name.ShouldBe(name);
            result.Password.ShouldBe(password);
            result.Marketplace.ShouldBe(marketplace);
            result.AuthenticationCode.ShouldBe(authenticationCode);
            result.Events.Count().ShouldBe(1);
            result.Events.SingleOrDefault().ShouldBeOfType<AuthenticationCodeUpdated>();
            // ReSharper disable once PossibleNullReferenceException
            ((AuthenticationCodeUpdated) result.Events.SingleOrDefault()).User.Id.Value.ShouldBe(id);
        }
    }
}