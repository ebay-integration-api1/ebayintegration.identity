﻿using System.Threading.Tasks;
using eBayMicroservices.Identity.Application.Commands;
using eBayMicroservices.Identity.Application.Commands.Handlers;
using eBayMicroservices.Identity.Application.Services;
using NSubstitute;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Application.Commands.Handlers
{
    public class RefreshUserAccessTokensHandlerTests
    {
        private readonly RefreshUserAccessTokensHandler _handler;
        private readonly ITokenRefreshExecutor _tokenRefreshExecutor;
        public RefreshUserAccessTokensHandlerTests()
        {
            _tokenRefreshExecutor = Substitute.For<ITokenRefreshExecutor>();
            _handler = new RefreshUserAccessTokensHandler(_tokenRefreshExecutor);
        }

        private Task Act(RefreshUserAccessTokens command) => _handler.HandleAsync(command);

        
        [Fact]
        public async Task given_command_should_be_succeed()
        {
            await Act(new RefreshUserAccessTokens());

            await _tokenRefreshExecutor.Received(1).ExecuteTokenRefresh();
        }
    }
}