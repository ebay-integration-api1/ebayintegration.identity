﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Application.Commands;
using eBayMicroservices.Identity.Application.Commands.Handlers;
using eBayMicroservices.Identity.Application.Exceptions.Concrete;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Core.Entities;
using eBayMicroservices.Identity.Core.Events.Abstract;
using eBayMicroservices.Identity.Core.Events.Concrete;
using eBayMicroservices.Identity.Core.Repositories;
using NSubstitute;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Application.Commands.Handlers
{
    public class UpdateUserAuthCodeHandlerTests
    {

        private Task Act(UpdateUserAuthCode command) => _updateUserAuthCodeHandler.HandleAsync(command); 
        private readonly UpdateUserAuthCodeHandler _updateUserAuthCodeHandler;
        private readonly IUserRepository _userRepository;
        private readonly IEventProcessor _eventProcessor;

        public UpdateUserAuthCodeHandlerTests()
        {
            _userRepository = Substitute.For<IUserRepository>();
            _eventProcessor = Substitute.For<IEventProcessor>();
            
            _updateUserAuthCodeHandler = new UpdateUserAuthCodeHandler(_userRepository, _eventProcessor);
        }

        [Fact]
        public async Task given_user_id_is_not_existing_user_should_throw_exception()
        {
            const string authenticationCode = "user_auth_token";
            
            Guid userId = Guid.NewGuid();
            
            UpdateUserAuthCode userAuthCode = new UpdateUserAuthCode(userId,authenticationCode);

            User user = null;
            // ReSharper disable once ExpressionIsAlwaysNull
            _userRepository.GetUserById(userId).Returns(user);

            Exception result = await Record.ExceptionAsync(async () => await Act(userAuthCode));
            
            result.ShouldNotBeNull();
            result.ShouldBeOfType<UserNotFoundException>();
            // ReSharper disable once PossibleNullReferenceException
            (result as UserNotFoundException).Identifier.ShouldBe(userId.ToString());
        }
        [Fact]
        public async Task given_valid_data_should_be_succeed()
        {
            const string name = "user_name_valid";
            const string login = "user_login_valid";
            const string password = "user_password_valid";
            const string authenticationCode = "user_auth_token";
            const string marketplace = "EBAY_PL";
            const bool isTestUser = false;
            
            Guid userId = Guid.NewGuid();
            
            User user = new User(userId,name,login,password,authenticationCode,null,null,marketplace,null,isTestUser);
            
            UpdateUserAuthCode userAuthCode = new UpdateUserAuthCode(userId,authenticationCode);

            _userRepository.GetUserById(userId).Returns(user);

            await Act(userAuthCode);

            await _userRepository.Received(1).UpdateUserAuthenticationCode(userAuthCode.UserId, userAuthCode.AuthorisationCode);
            await _eventProcessor.Received()
                .ProcessAsync(Arg.Is<IList<IDomainEvent>>(x => ((AuthenticationCodeUpdated)x.FirstOrDefault()).User.Id.Value == userId));
        }
        
        
    }
}