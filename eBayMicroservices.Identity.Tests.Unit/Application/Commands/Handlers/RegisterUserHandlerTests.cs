﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Application.Commands;
using eBayMicroservices.Identity.Application.Commands.Handlers;
using eBayMicroservices.Identity.Application.Exceptions.Concrete;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Core.Entities;
using eBayMicroservices.Identity.Core.Events.Abstract;
using eBayMicroservices.Identity.Core.Events.Concrete;
using eBayMicroservices.Identity.Core.Repositories;
using NSubstitute;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Application.Commands.Handlers
{
    public class RegisterUserHandlerTests
    {
        private readonly RegisterUserHandler _handler;
        private readonly IUserRepository _userRepository;
        private readonly IEventProcessor _eventProcessor;
        private readonly IMarketplaceRepository _marketplaceRepository;

        public RegisterUserHandlerTests()
        {
            _userRepository = Substitute.For<IUserRepository>();
            _eventProcessor = Substitute.For<IEventProcessor>();
            _marketplaceRepository = Substitute.For<IMarketplaceRepository>();
            _handler = new RegisterUserHandler(_userRepository,_eventProcessor,_marketplaceRepository);
        }

        private async Task Act(RegisterUser registerUser) => await _handler.HandleAsync(registerUser);

        [Fact]
        public async Task given_data_for_existing_user_should_throw_an_exception()
        {
            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = "user_login_valid";
            const string password = "user_password_valid";
            const string authenticationCode = "user_auth_code";
            const string marketplace = "EBAY_PL";
            const bool testUser = false;

            RegisterUser registerUser = new RegisterUser(id, login, name, password, marketplace,authenticationCode,testUser);

            _userRepository.ExistsAsync(registerUser.Login).Returns(true);

            Exception ex = await Record.ExceptionAsync(async () => await Act(registerUser));
            
            ex.ShouldNotBeNull();
            
            ex.ShouldBeOfType<UserAlreadyExists>();

            UserAlreadyExists exc = (UserAlreadyExists)ex;
            exc.Login.ShouldBe(registerUser.Login);
        }
        
        [Fact]
        public async Task given_data_for_not_existing_marketplace_should_throw_an_exception()
        {
            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = "user_login_valid";
            const string password = "user_password_valid";
            const string authenticationCode = "user_auth_code";
            const string marketplace = "not_existing_marketplace";
            const bool testUser = false;

            RegisterUser registerUser = new RegisterUser(id, login, name, password, marketplace,authenticationCode,testUser);

            _userRepository.ExistsAsync(registerUser.Login).Returns(false);
            _marketplaceRepository.ExistsAsync(marketplace).Returns(false);

            Exception ex = await Record.ExceptionAsync(async () => await Act(registerUser));
            
            ex.ShouldNotBeNull();
            
            ex.ShouldBeOfType<NotExistingMarketplaceWhileSigningUpUser>();

            NotExistingMarketplaceWhileSigningUpUser exc = (NotExistingMarketplaceWhileSigningUpUser)ex;
            exc.Marketplace.ShouldBe(marketplace);
        }
        
        
        [Fact]
        public async Task given_valid_data_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = "user_login_valid";
            const string password = "user_password_valid";
            const string authenticationCode = "user_auth_code";
            const bool testUser = false;
            const string marketplace = "EBAY_PL";

            RegisterUser registerUser = new RegisterUser(id, login, name, password, marketplace,authenticationCode,testUser);

            _userRepository.ExistsAsync(login).Returns(false);

            _marketplaceRepository.ExistsAsync(marketplace).Returns(true);
            
            await Act(registerUser);

            await _userRepository.Received().AddAsync(
                Arg.Is<User>(
                    x =>
                        x.Login == login
                        && x.Name == name
                        && x.Password == password
                        && x.AuthenticationCode == authenticationCode
                ));
            await _eventProcessor.Received().ProcessAsync(
                Arg.Is<IList<IDomainEvent>>(
                    x=> 
                        ((UserCreated)x.FirstOrDefault()).User.Id.Value == id
            && ((UserCreated)x.FirstOrDefault()).User.Login == login
            && ((UserCreated)x.FirstOrDefault()).User.Name == (name)
            && ((UserCreated)x.FirstOrDefault()).User.Password ==password
            && ((UserCreated)x.FirstOrDefault()).User.AuthenticationCode==authenticationCode
            && ((UserCreated)x.FirstOrDefault()).User.Events.Count()==1
                                           // ReSharper disable once PossibleNullReferenceException
            &&((UserCreated) ((UserCreated)x.FirstOrDefault()).User.Events.SingleOrDefault()).User.Id.Value == id
                        ));
        }
    }
}