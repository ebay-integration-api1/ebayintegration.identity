﻿using eBayMicroservices.Identity.Application.Commands;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Application.Commands
{
    public class RefreshUserAccessTokensTests
    {
        private RefreshUserAccessTokens Act() => new RefreshUserAccessTokens();


        [Fact]
        public void given_valid_data_should_be_succeed()
        {
            RefreshUserAccessTokens result = Act();
            
            result.ShouldNotBeNull();
        }
    }
}