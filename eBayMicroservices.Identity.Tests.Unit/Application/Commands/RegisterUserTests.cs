﻿using System;
using eBayMicroservices.Identity.Application.Commands;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Application.Commands
{
    public class RegisterUserTests
    {
        private static RegisterUser Act(Guid id, string login, string name, string password,
            string marketplace,string authenticationCode,bool isTestUser)
            => new RegisterUser(id, login, name, password, marketplace,authenticationCode,isTestUser);

        [Fact]
        public void given_valid_data_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = "user_login_valid";
            const string authenticationCode = "user_eshop_authenticationCode";
            const string password = "user_password_valid";
            const bool testUser = true;
            const string marketplace = "EBAY_PL";

            RegisterUser result = Act(id,login,name,password,marketplace,authenticationCode,testUser);
            
            result.ShouldNotBeNull();
            result.ShouldNotBeNull();
            result.Id.ShouldBe(id);
            result.Login.ShouldBe(login);
            result.Name.ShouldBe(name);
            result.Password.ShouldBe(password);
            result.Marketplace.ShouldBe(marketplace);
            result.AuthenticationCode.ShouldBe(authenticationCode);
            result.IsTestUser.ShouldBe(testUser);
        }
        [Fact]
        public void given_valid_data_with_production_user_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = "user_login_valid";
            const string authenticationCode = "user_eshop_authenticationCode";
            const string password = "user_password_valid";
            const bool testUser = false;
            const string marketplace = "EBAY_PL";

            RegisterUser result = Act(id,login,name,password,marketplace,authenticationCode,testUser);
            
            result.ShouldNotBeNull();
            result.ShouldNotBeNull();
            result.Id.ShouldBe(id);
            result.Login.ShouldBe(login);
            result.Name.ShouldBe(name);
            result.Password.ShouldBe(password);
            result.Marketplace.ShouldBe(marketplace);
            result.AuthenticationCode.ShouldBe(authenticationCode);
            result.IsTestUser.ShouldBe(testUser);
        }
    }
}