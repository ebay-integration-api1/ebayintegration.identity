﻿using eBayMicroservices.Identity.Application.Commands;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Application.Commands
{
    public class SignInTests
    {
        private static SignIn Act(string login,string password) => new SignIn(login,password);

        [Fact]
        public void given_valid_data_should_be_success()
        {
            const string login = "valid_login";
            const string password = "valid_password";

            SignIn result = Act(login, password);
            
            result.ShouldNotBeNull();
            result.Login.ShouldBe(login);
            result.Password.ShouldBe(password);
        }
    }
}