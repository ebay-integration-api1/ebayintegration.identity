﻿using System;
using eBayMicroservices.Identity.Application.Commands;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Application.Commands
{
    public class UpdateUserAuthCodeTests
    {
        private UpdateUserAuthCode Act(Guid userId,string authCode) => new UpdateUserAuthCode(userId,authCode);


        [Fact]
        public void given_valid_data_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            const string authCode = "valid_auth_code";
            
            UpdateUserAuthCode result = Act(id, authCode);
            
            result.ShouldNotBeNull();
            result.UserId.ShouldBe(id);
            result.AuthorisationCode.ShouldBe(authCode);
        }
    }
}