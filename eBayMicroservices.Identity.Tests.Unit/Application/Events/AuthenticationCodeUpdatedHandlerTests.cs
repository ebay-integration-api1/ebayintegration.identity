﻿using System;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Application.Events.Handlers;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Core.Entities;
using eBayMicroservices.Identity.Core.Events.Concrete;
using NSubstitute;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Application.Events
{
    public class AuthenticationCodeUpdatedHandlerTests
    {
        private readonly AuthenticationCodeUpdatedHandler _handler;
        private readonly ITokenRefreshExecutor _tokenRefreshExecutor;
        public AuthenticationCodeUpdatedHandlerTests()
        {
            _tokenRefreshExecutor = Substitute.For<ITokenRefreshExecutor>();
            _handler = new AuthenticationCodeUpdatedHandler(_tokenRefreshExecutor);
        }

        private Task Act(AuthenticationCodeUpdated @event) => _handler.HandleAsync(@event);

        
        [Fact]
        public async Task given_command_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = "user_login_valid";
            const string password = "user_password_valid";
            const string authenticationCode = "user_auth_token";
            const string marketplace = "EBAY_PL";
            const bool isTestUser = false;
            

            User result = new User(id,name,login,password,authenticationCode,null,null,marketplace,null,isTestUser);

            
            await Act(new AuthenticationCodeUpdated(result));

            await _tokenRefreshExecutor.Received(1).ExecuteTokenRefresh(result);
        }
    }
}