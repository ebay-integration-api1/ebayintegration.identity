﻿

using System;
using eBayMicroservices.Identity.Application.Queries;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Application.Queries
{
    public class GetUserTests
    {
        private static GetUser Act(Guid id) => new GetUser
        {
            Id = id
        };

        [Fact]
        public void create_query_should_be_succeed()
        {
            Guid id = Guid.NewGuid();

            GetUser result = Act(id);
            
            result.ShouldNotBeNull();
            result.Id.ShouldBe(id);
        }
    }
}