﻿using eBayMicroservices.Identity.Application.Queries;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Application.Queries
{
    public class GetMarketplacesTests
    {
        private static GetMarketplaces Act() => new GetMarketplaces();

        [Fact]
        public void created_query_should_be_succeed()
        {
            GetMarketplaces result = Act();
            
            result.ShouldNotBeNull();
        }
        
    }
}