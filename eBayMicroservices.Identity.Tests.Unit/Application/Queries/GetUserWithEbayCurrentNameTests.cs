﻿using System;
using eBayMicroservices.Identity.Application.Queries;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Application.Queries
{
    public class GetUserWithEbayCurrentNameTests
    {
        private static GetUserWithEbayCurrentName Act(Guid id) => new GetUserWithEbayCurrentName
        {
            Id = id
        };

        [Fact]
        public void create_query_should_be_succeed()
        {
            Guid id = Guid.NewGuid();

            GetUserWithEbayCurrentName result = Act(id);
            
            result.ShouldNotBeNull();
            result.Id.ShouldBe(id);
        }
    }
}