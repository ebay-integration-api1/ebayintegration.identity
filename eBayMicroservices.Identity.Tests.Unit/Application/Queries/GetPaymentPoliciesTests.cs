﻿using System;
using eBayMicroservices.Identity.Application.Queries;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Application.Queries
{
    public class GetPaymentPoliciesTests 
    {
        private static GetPaymentPolicies Act(string marketPlace,Guid userId) => new GetPaymentPolicies
        {
            Marketplace = marketPlace, UserId = userId
        };

        [Fact]
        public void given_valid_data_should_be_succeed()
        {
            const string marketplace = "test";
            Guid userId = Guid.NewGuid();

            GetPaymentPolicies result = Act(marketplace, userId);
            
            result.ShouldNotBeNull();
            result.UserId.ShouldBe(userId);
            result.Marketplace.ShouldBe(marketplace);
        }
    }
}