﻿using eBayMicroservices.Identity.Application.Queries;
using Shouldly;
using Xunit;
// ReSharper disable ConditionIsAlwaysTrueOrFalse

// ReSharper disable ExpressionIsAlwaysNull

namespace eBayMicroservices.Identity.Tests.Unit.Application.Queries
{
    public class GetUrlToSignInTests
    {
        private static GetUrlToSignIn Act(bool? sandbox) => new GetUrlToSignIn
        {
            IsSandbox = sandbox
        };

        [Fact]
        public void given_null_should_contain_null()
        {
            bool? sandbox = null;

            GetUrlToSignIn p = Act(sandbox);
            
            p.IsSandbox.ShouldBe(sandbox);
        }
        
        [Fact]
        public void given_false_should_contain_null()
        {
            bool? sandbox = false;

            GetUrlToSignIn p = Act(sandbox);
            
            p.IsSandbox.ShouldBe(sandbox);
        }
        
        [Fact]
        public void given_true_should_contain_null()
        {
            bool? sandbox = true;

            GetUrlToSignIn p = Act(sandbox);
            
            p.IsSandbox.ShouldBe(sandbox);
        }
    }
}