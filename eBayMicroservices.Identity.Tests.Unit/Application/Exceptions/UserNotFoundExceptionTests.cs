﻿using System;
using eBayMicroservices.Identity.Application.Exceptions.Concrete;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Application.Exceptions
{
    public class UserNotFoundExceptionTests
    {
        private static UserNotFoundException Act(string identifier) => new UserNotFoundException(identifier);

        [Fact]
        public void given_Guid_identifier_should_create_exception()
        {
            Guid id = Guid.NewGuid();

            UserNotFoundException result = Act(id.ToString());
            
            result.Identifier.ShouldBe(id.ToString());
        }
        [Fact]
        public void given_string_identifier_should_create_exception()
        {
            const string identifier = "identifier";

            UserNotFoundException result = Act(identifier);
            
            result.Identifier.ShouldBe(identifier);
        }
        
    }
}