﻿using eBayMicroservices.Identity.Application.Exceptions.Concrete;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Application.Exceptions
{
    public class EBayEnvironmentNotSpecifiedTests
    {
        private static EBayEnvironmentNotSpecified Act() => new EBayEnvironmentNotSpecified();

        [Fact]
        public void create_exception_should_be_succeed()
        {
            EBayEnvironmentNotSpecified ex = Act();
            ex.ShouldNotBeNull();
        }
    }
}