﻿using eBayMicroservices.Identity.Application.Exceptions.Concrete;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Application.Exceptions
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class UnauthorisedUserExceptionTests
    {
        private static UnauthorisedUserException Act() => new UnauthorisedUserException();

        [Fact]
        public static void create_exception_should_be_succeed()
        {
            UnauthorisedUserException result = Act();
            result.ShouldNotBeNull();
        }
    }
}