﻿using eBayMicroservices.Identity.Application.Exceptions.Concrete;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Application.Exceptions
{
    public class UserAlreadyExistsTests
    {
        private static UserAlreadyExists Act(string login) => new UserAlreadyExists(login);

        [Fact]
        public void given_login_should_create_exception()
        {
            const string login = "valid_login";

            UserAlreadyExists result = Act(login);
            result.ShouldNotBeNull();
            result.Login.ShouldBe(login);
        }
    }
}