﻿using System;
using Convey.CQRS.Events;
using eBayMicroservices.Identity.Core.Entities;
using eBayMicroservices.Identity.Core.Events.Abstract;
using eBayMicroservices.Identity.Core.Events.Concrete;
using eBayMicroservices.Identity.Infrastructure.Services;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Infrastructure.Services
{
    public class DomainToIntegrationEventMapperTests
    {
        private readonly DomainToIntegrationEventMapper _domainToIntegrationEventMapper;

        public DomainToIntegrationEventMapperTests()
        {
            _domainToIntegrationEventMapper = new DomainToIntegrationEventMapper();
        }

        private IEvent SingleAct(IDomainEvent domainEvent) => _domainToIntegrationEventMapper.Map(domainEvent);

        [Fact]
        public void give_undefined_domain_event_should_return_null()
        {
            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = "user_login_valid";
            const string password = "user_password_valid";
            const string authenticationCode = "user_auth_token";
            const string marketplace = "EBAY_PL";
            const bool isTestUser = false;
            User user = User.Create(id, name, login, password,authenticationCode, marketplace,isTestUser);
            
            IDomainEvent @event = new UserCreated(user);

            IEvent result = SingleAct(@event);
            
            result.ShouldBeNull();
        }
    }
}