﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Convey.CQRS.Events;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Core.Events.Abstract;
using eBayMicroservices.Identity.Infrastructure.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Xunit;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace eBayMicroservices.Identity.Tests.Unit.Infrastructure.Services
{
    public class EventProcessorTests
    {
        private readonly EventProcessor _eventProcessor;
        private readonly ILogger<IEventProcessor> _logger;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IDomainToIntegrationEventMapper _domainToIntegrationEventMapper;
        private readonly IMessageBroker _messageBroker;

        private async Task Act_ProcessAsync(IEnumerable<IDomainEvent> events) =>
            await _eventProcessor.ProcessAsync(events);
        
        public EventProcessorTests()
        {
            _logger = Substitute.For<ILogger<IEventProcessor>>();
            _serviceScopeFactory = Substitute.For<IServiceScopeFactory>();
            _domainToIntegrationEventMapper = Substitute.For<IDomainToIntegrationEventMapper>();
            _messageBroker = Substitute.For<IMessageBroker>();
            _eventProcessor = new EventProcessor(_logger,_serviceScopeFactory,_domainToIntegrationEventMapper,_messageBroker);
        }
        [Fact]
        public async Task give_no_events_should_be_succeed()
        {
            IEnumerable<IDomainEvent> events = Enumerable.Empty<IDomainEvent>();

            await Act_ProcessAsync(events);

            await _messageBroker.DidNotReceive().PublishAsync(Arg.Any<IEnumerable<IEvent>>());
        }

    }
}