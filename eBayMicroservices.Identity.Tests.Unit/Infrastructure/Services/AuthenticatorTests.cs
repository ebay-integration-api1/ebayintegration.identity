﻿using System;
using eBayMicroservices.Identity.Application.Exceptions.Concrete;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Infrastructure.Services;
using Microsoft.AspNetCore.Http;
using NSubstitute;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Infrastructure.Services
{
    public class AuthenticatorTests
    {
        private readonly IAuthenticator _authenticator;
        private readonly IIdentityProvider _identityProvider;

        public AuthenticatorTests()
        {

            _identityProvider = Substitute.For<IIdentityProvider>();
            _authenticator = new Authenticator(_identityProvider);
        }
        private void Act(HttpContext ctx) => _authenticator.Authenticate(ctx);
        
        [Fact]
        public void given_valid_context_should_be_succeed()
        {
            HttpContext ctx = Substitute.For<HttpContext>();

            _identityProvider.GetIdentityAsString(Arg.Any<HttpContext>()).Returns(Guid.NewGuid().ToString());
            
            Act(ctx);

        }
        
        [Fact]
        public void given_invalid_context_should_throw_an_exception()
        {
            HttpContext ctx = Substitute.For<HttpContext>();

            _identityProvider.GetIdentityAsString(Arg.Any<HttpContext>()).Returns("not_guid");
            
            Exception ex = Record.Exception(() => Act(ctx));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<UnauthorisedUserException>();
        }
    }
}