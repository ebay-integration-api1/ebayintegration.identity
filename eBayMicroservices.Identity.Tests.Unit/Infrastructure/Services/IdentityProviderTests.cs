﻿using System;
using System.Collections.Generic;
using eBayMicroservices.Identity.Infrastructure.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using NSubstitute;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Infrastructure.Services
{
    public class IdentityProviderTests
    {
        private readonly IdentityProvider _identityProvider;

        public IdentityProviderTests()
        {
            _identityProvider = new IdentityProvider();
        }

        private string Act_AsString(HttpContext ctx) => _identityProvider.GetIdentityAsString(ctx);
        private Guid Act_AsGuid(HttpContext ctx) => _identityProvider.GetIdentity(ctx);

        [Fact]
        public void given_valid_ctx_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            string idString = id.ToString();
            HttpContext con = Substitute.For<HttpContext>();
            HttpRequest req = Substitute.For<HttpRequest>();
            con.Request.Returns(req);
            req.Headers.Returns(new HeaderDictionary()
            {
                new KeyValuePair<string, StringValues>
                ("Authorization", new StringValues(idString))
            });

            string result = Act_AsString(con);
            
            result.ShouldBe(idString);
        }
        
        [Fact]
        public void given_valid_ctx_guid_act_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            string identifier = id.ToString();
            HttpContext con = Substitute.For<HttpContext>();
            HttpRequest req = Substitute.For<HttpRequest>();
            con.Request.Returns(req);
            req.Headers.Returns(new HeaderDictionary()
            {
                new KeyValuePair<string, StringValues>
                    ("Authorization", new StringValues(identifier))
            });

            Guid result = Act_AsGuid(con);
            
            result.ToString().ShouldBe(identifier);
        }
    }
}