﻿using System;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Application.Commands;
using eBayMicroservices.Identity.Application.Exceptions.Concrete;
using eBayMicroservices.Identity.Core.Entities;
using eBayMicroservices.Identity.Core.Exceptions;
using eBayMicroservices.Identity.Core.Repositories;
using eBayMicroservices.Identity.Infrastructure.Services;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Shouldly;
using Xunit;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace eBayMicroservices.Identity.Tests.Unit.Infrastructure.Services
{
    public class IdentityServiceTests
    {
        private readonly IdentityService _identityService;
        private readonly ILogger<IdentityService> _logger;
        private readonly IUserRepository _userRepository;

        public IdentityServiceTests()
        {
            _userRepository = Substitute.For<IUserRepository>();
            _logger = Substitute.For<ILogger<IdentityService>>();
            
            _identityService = new IdentityService(_userRepository,_logger);
            
        }

        private async Task Act(SignIn command) => await _identityService.SignInAsync(command);

        [Fact]
        public async Task empty_login_should_throw_exception()
        {
            const string login = "";
            const string password = "valid_password";
            SignIn signIn = new SignIn(login,password);

            Exception ex = await Record.ExceptionAsync(async () => await Act(signIn));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<InvalidUserDataException>();

            InvalidUserDataException typedEx = ex as InvalidUserDataException;

            typedEx.Field.ShouldBe(nameof(signIn.Login));
        }
        
        [Fact]
        public async Task null_login_should_throw_exception()
        {
            const string login = null;
            const string password = "valid_password";
            SignIn signIn = new SignIn(login,password);

            Exception ex = await Record.ExceptionAsync(async () => await Act(signIn));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<InvalidUserDataException>();

            InvalidUserDataException typedEx = ex as InvalidUserDataException;

            typedEx.Field.ShouldBe(nameof(signIn.Login));
        }
        
        [Fact]
        public async Task empty_password_should_throw_exception()
        {
            const string login = "valid_login";
            const string password = "";
            SignIn signIn = new SignIn(login,password);

            Exception ex = await Record.ExceptionAsync(async () => await Act(signIn));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<InvalidUserDataException>();

            InvalidUserDataException typedEx = ex as InvalidUserDataException;

            typedEx.Field.ShouldBe(nameof(signIn.Password));
        }
        
        [Fact]
        public async Task null_password_should_throw_exception()
        {
            const string login = "valid_login";
            const string password = null;
            SignIn signIn = new SignIn(login,password);

            Exception ex = await Record.ExceptionAsync(async () => await Act(signIn));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<InvalidUserDataException>();

            InvalidUserDataException typedEx = ex as InvalidUserDataException;

            typedEx.Field.ShouldBe(nameof(signIn.Password));
        }
        
        [Fact]
        public async Task valid_data_but_not_existing_user_should_throw_exception()
        {
            const string login = "valid_login";
            const string password = "valid_password";
            SignIn signIn = new SignIn(login,password);

            User user = null;
            
            // ReSharper disable once ExpressionIsAlwaysNull
            _userRepository.GetUserByLogin(signIn.Login).Returns(user);
            
            Exception ex = await Record.ExceptionAsync(async () => await Act(signIn));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<UserNotFoundException>();

            UserNotFoundException typedEx = ex as UserNotFoundException;

            typedEx.Identifier.ShouldBe(signIn.Login);
        }
        
        [Fact]
        public async Task valid_login_and_password_but_incorrect()
        {
            const string givenLogin = "valid_login";
            const string givenPassword = "incorrect_password";
            SignIn signIn = new SignIn(givenLogin,givenPassword);

            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = "user_login_valid";
            const string password = "user_password_valid";
            const string authenticationCode = "user_auth_token";
            const string marketplace = "EBAY_PL";
            const bool isTestUser = false;

            User user = User.Create(id, name, login, password, authenticationCode, marketplace,isTestUser);
            
            _userRepository.GetUserByLogin(signIn.Login).Returns(user);
            
            Exception ex = await Record.ExceptionAsync(async () => await Act(signIn));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<InvalidUserDataException>();

            InvalidUserDataException typedEx = ex as InvalidUserDataException;

            // ReSharper disable once PossibleNullReferenceException
            typedEx.Field.ShouldBe(nameof(signIn.Password));
        }
        
        [Fact]
#pragma warning disable 1998
        public async Task valid_login_and_password_should_be_succeed()
#pragma warning restore 1998
        {
            const string givenLogin = "user_login_valid";
            const string givenPassword = "user_password_valid";
            SignIn signIn = new SignIn(givenLogin,givenPassword);

            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = "user_login_valid";
            const string password = "user_password_valid";
            const string authenticationCode = "user_token_valid";
            const string marketplace = "EBAY_PL";
            const bool isTestUser = true;

            User user = User.Create(id, name, login, password, authenticationCode, marketplace,isTestUser);
            
            _userRepository.GetUserByLogin(signIn.Login).Returns(user);
            
        }
        
    }
}