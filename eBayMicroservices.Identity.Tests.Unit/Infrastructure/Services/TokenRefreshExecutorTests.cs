﻿using System;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Core.Entities;
using eBayMicroservices.Identity.Core.Repositories;
using eBayMicroservices.Identity.Infrastructure.Services;
using eBayMicroservices.Identity.Infrastructure.Services.Responses;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Infrastructure.Services
{
    public class TokenRefreshExecutorTests
    {
        private readonly ITokenRefreshExecutor _tokenRefreshExecutor;
        private readonly IUserRepository _userRepository;
        private readonly IEBayRefreshTokenCallExecutor _eBayRefreshTokenCallExecutor;
        // ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
        private readonly ILogger<TokenRefreshExecutor> _logger;

        public TokenRefreshExecutorTests()
        {
            _userRepository = Substitute.For<IUserRepository>();
            _eBayRefreshTokenCallExecutor = Substitute.For<IEBayRefreshTokenCallExecutor>();
            _logger = Substitute.For<ILogger<TokenRefreshExecutor>>();
            _tokenRefreshExecutor = new TokenRefreshExecutor(_userRepository,_eBayRefreshTokenCallExecutor,_logger);
        }

        private Task Act_WithUser(User user) => _tokenRefreshExecutor.ExecuteTokenRefresh(user);
        private Task Act_WithoutUser() => _tokenRefreshExecutor.ExecuteTokenRefresh();

        [Fact]
        public async Task given_valid_for_user_data_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = "user_login_valid";
            const string password = "user_password_valid";
            const string authenticationCode = "user_auth_token";
            const string accessToken = "user_auth_token";
            const string refreshToken = "user_auth_token";
            const string marketplace = "EBAY_PL";
            const long dateTime = 12412123;
            const bool isTestUser = true;
            
            User user = new User(id,name,login,password,authenticationCode,accessToken,refreshToken,marketplace, dateTime,isTestUser);

            _eBayRefreshTokenCallExecutor.CreateFirstTokens(user.AuthenticationCode,isTestUser).Returns(new EBayRefreshTokenCallResponse(accessToken,dateTime.ToString(),refreshToken));

            
            await Act_WithUser(user);
            
            
            await _eBayRefreshTokenCallExecutor.Received(1).CreateFirstTokens(authenticationCode,isTestUser);
            
            await _userRepository.Received(1).UpdateUserTokens(id,accessToken,refreshToken,dateTime.ToString());
        }
        
        [Fact]
        public async Task given_valid_data_for_user_but_ebay_error_should_be_not_succeed()
        {
            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = "user_login_valid";
            const string password = "user_password_valid";
            const string authenticationCode = "user_auth_token";
            const string accessToken = "user_auth_token";
            const string refreshToken = "user_auth_token";
            const string marketplace = "EBAY_PL";
            long dateTime = 12412123;
            const bool isTestUser = true;
            
            User user = new User(id,name,login,password,authenticationCode,accessToken,refreshToken,marketplace, dateTime,isTestUser);

            _eBayRefreshTokenCallExecutor.CreateFirstTokens(user.AuthenticationCode,isTestUser).Returns(new EBayRefreshTokenCallResponse());

            
            await Act_WithUser(user);
            
            
            await _eBayRefreshTokenCallExecutor.Received(1).CreateFirstTokens(authenticationCode,isTestUser);
            
            await _userRepository.DidNotReceiveWithAnyArgs().UpdateUserTokens(Arg.Any<Guid>(),Arg.Any<string>(),Arg.Any<string>(),Arg.Any<string>());
        }
        [Fact]
        public async Task given_valid_data_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = "user_login_valid";
            const string password = "user_password_valid";
            const string authenticationCode = "user_auth_token";
            const string accessToken = "user_auth_token";
            const string refreshToken = "user_auth_token";
            const string marketplace = "EBAY_PL";
            long dateTime = 12412123;
            const bool isTestUser = true;
            
            User user = new User(id,name,login,password,authenticationCode,accessToken,refreshToken,marketplace, dateTime,isTestUser);
            User emptyUser = null;
            
            
            _eBayRefreshTokenCallExecutor.RefreshTokens(user.AuthenticationCode,isTestUser).Returns(new EBayRefreshTokenCallResponse(accessToken,dateTime.ToString(),refreshToken));

            // ReSharper disable once ExpressionIsAlwaysNull
            _userRepository.GetUserToRefreshToken().Returns(user,emptyUser);
            
            await Act_WithoutUser();
            
            
            await _eBayRefreshTokenCallExecutor.Received(1).RefreshTokens(authenticationCode,isTestUser);
            
            await _userRepository.Received(1).UpdateUserTokens(id, accessToken, Arg.Any<string>());
        }
        [Fact]
        public async Task given_valid_data_but_ebay_error_should_not_be_succeed()
        {
            Guid id = Guid.NewGuid();
            const string name = "user_name_valid";
            const string login = "user_login_valid";
            const string password = "user_password_valid";
            const string authenticationCode = "user_auth_token";
            const string accessToken = "user_auth_token";
            const string refreshToken = "user_auth_token";
            const string marketplace = "EBAY_PL";
            long dateTime = 12412123;
            const bool isTestUser = true;
            
            User user = new User(id,name,login,password,authenticationCode,accessToken,refreshToken,marketplace, dateTime,isTestUser);
            User emptyUser = null;
            
            _eBayRefreshTokenCallExecutor.RefreshTokens(user.AuthenticationCode,isTestUser).Returns(new EBayRefreshTokenCallResponse());

            // ReSharper disable once ExpressionIsAlwaysNull
            _userRepository.GetUserToRefreshToken().Returns(user,emptyUser);
            
            await Act_WithoutUser();
            
            
            await _eBayRefreshTokenCallExecutor.Received(1).RefreshTokens(authenticationCode,isTestUser);
            
            await _userRepository.DidNotReceiveWithAnyArgs().UpdateUserTokens(id, accessToken, refreshToken, Arg.Any<string>());
        }
    }
}