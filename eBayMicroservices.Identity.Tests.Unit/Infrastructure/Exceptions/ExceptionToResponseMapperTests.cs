﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using Convey.WebApi.Exceptions;
using eBayMicroservices.Identity.Application.Exceptions.Concrete;
using eBayMicroservices.Identity.Core.Exceptions;
using eBayMicroservices.Identity.Infrastructure.Exceptions;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Infrastructure.Exceptions
{
    public class ExceptionToResponseMapperTests
    {
        private readonly ExceptionToResponseMapper _mapper;

        public ExceptionToResponseMapperTests()
        {
            _mapper = new ExceptionToResponseMapper();
        }

        private ExceptionResponse Act(Exception exception) => _mapper.Map(exception);

        [Fact]
        public void given_domain_exception_should_be_succeed()
        {
            Guid invalidId = Guid.NewGuid();
            InvalidAggregateIdException exception = new InvalidAggregateIdException(invalidId);

            ExceptionResponse p = Act(exception);

            dynamic st = p.Response;

            ExpandoObject unpackedResponse = UnpackResponse(st);

            KeyValuePair<string, object> code = unpackedResponse.FirstOrDefault(x => x.Key == "code");
            KeyValuePair<string, object> reason = unpackedResponse.FirstOrDefault(x => x.Key == "reason");
            
            code.Value.ShouldBe(exception.Code);
            reason.Value.ShouldBe(exception.Message);
        }
        [Fact]
        public void given_domain_appException_should_be_succeed()
        {
            const string login = "valid_and_existing_login";
            UserAlreadyExists exception = new UserAlreadyExists(login);

            ExceptionResponse p = Act(exception);

            dynamic st = p.Response;

            ExpandoObject unpackedResponse = UnpackResponse(st);

            KeyValuePair<string, object> code = unpackedResponse.FirstOrDefault(x => x.Key == "code");
            KeyValuePair<string, object> reason = unpackedResponse.FirstOrDefault(x => x.Key == "reason");
            
            code.Value.ShouldBe(exception.Code);
            reason.Value.ShouldBe(exception.Message);
        }
        private static ExpandoObject UnpackResponse(dynamic st)
        {
            IDictionary<string, object> expando = new ExpandoObject();

            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(st.GetType()))
                expando.Add(property.Name, property.GetValue(st));

            var zzz = (ExpandoObject) expando;
            return zzz;
        }
    }
}