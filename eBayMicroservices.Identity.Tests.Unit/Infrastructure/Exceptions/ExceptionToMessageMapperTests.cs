﻿using System;
using eBayMicroservices.Identity.Infrastructure.Exceptions;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Infrastructure.Exceptions
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class ExceptionToMessageMapperTests
    {
        private readonly ExceptionToMessageMapper _exceptionToResponseMapper;

        public ExceptionToMessageMapperTests()
        {
            _exceptionToResponseMapper = new ExceptionToMessageMapper();
        }

        private object Act(Exception ex,string message) => _exceptionToResponseMapper.Map(ex,message);

        [Fact]
        public void undefined_exception_in_mapper_should_return_null()
        {
            Exception ex = new Exception("ex");

            object result = Act(ex, "message");
            
            result.ShouldBeNull();
        }
        
        public object Map(Exception exception, object message)
            => exception switch
            {
                _ => null
            };
    }
}