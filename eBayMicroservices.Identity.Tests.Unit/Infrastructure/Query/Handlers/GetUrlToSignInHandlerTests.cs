﻿using System;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Application.Exceptions.Concrete;
using eBayMicroservices.Identity.Application.Queries;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Infrastructure.Mongo.Queries.Handlers;
using NSubstitute;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Infrastructure.Query.Handlers
{
    public class GetUrlToSignInHandlerTests
    {
        private Task<string> Act(GetUrlToSignIn query) => _handler.HandleAsync(query);
        private readonly GetUrlToSignInHandler _handler;
        private readonly IRedirectUriToAuthorizeInEBayProvider _redirectUriToAuthorizeInEBayProvider;

        public GetUrlToSignInHandlerTests()
        {
            _redirectUriToAuthorizeInEBayProvider = Substitute.For<IRedirectUriToAuthorizeInEBayProvider>();
            _handler = new GetUrlToSignInHandler(_redirectUriToAuthorizeInEBayProvider);
        }

        [Fact]
        public async Task given_null_query_sandbox_should_throw_an_exception()
        {
            GetUrlToSignIn query = new GetUrlToSignIn
            {
                IsSandbox = null
            };

            Exception ex = await Record.ExceptionAsync(async () => await Act(query));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<EBayEnvironmentNotSpecified>();
        }
        
        [Fact]
        public async Task given_false_query_sandbox_should_be_succeed()
        {
            GetUrlToSignIn query = new GetUrlToSignIn
            {
                IsSandbox = true
            };

            _redirectUriToAuthorizeInEBayProvider.UrlToEBayAuthorize(true).Returns("zzzzz.sandbox.zzzz");
            string result =  await Act(query);
            
            result.ShouldNotBeNull();
            result.ShouldContain("sandbox");
        }
        
        [Fact]
        public async Task given_true_query_sandbox_should_be_succeed()
        {
            GetUrlToSignIn query = new GetUrlToSignIn
            {
                IsSandbox = false
            };

            _redirectUriToAuthorizeInEBayProvider.UrlToEBayAuthorize(true).Returns("zzzzz.zzzz");
            string result =  await Act(query);
            
            result.ShouldNotBeNull();
            result.ShouldNotContain("sandbox");
        }
    }
}