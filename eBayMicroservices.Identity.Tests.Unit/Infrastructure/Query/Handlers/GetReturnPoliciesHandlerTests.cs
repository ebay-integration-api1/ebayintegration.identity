﻿using System;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Application.Exceptions.Concrete;
using eBayMicroservices.Identity.Application.Queries;
using eBayMicroservices.Identity.Application.Services;
using eBayMicroservices.Identity.Core.Entities;
using eBayMicroservices.Identity.Core.Repositories;
using eBayMicroservices.Identity.Infrastructure.Mongo.Queries.Handlers;
using NSubstitute;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.Unit.Infrastructure.Query.Handlers
{
    
    
    
    public class GetReturnPoliciesHandlerTests
    {
        private readonly IUserRepository _userRepository;
        private readonly IEBayPoliciesServiceClient _ebayPoliciesServiceClient;
        private readonly GetReturnPoliciesHandler _handler;

        public GetReturnPoliciesHandlerTests()
        {
            _userRepository = Substitute.For<IUserRepository>();
            _ebayPoliciesServiceClient = Substitute.For<IEBayPoliciesServiceClient>();
            _handler = new GetReturnPoliciesHandler(_userRepository,_ebayPoliciesServiceClient);
        }

        private Task Act(GetReturnPolicies query) => _handler.HandleAsync(query);

        [Fact]
        public async Task given_empty_marketplace_should_throw_an_exception()
        {
            const string marketplace = "";
            Guid id = Guid.NewGuid();

            GetReturnPolicies getFulfillmentPolicies = new GetReturnPolicies
            {
                Marketplace = marketplace,
                UserId = id
            };

            Exception ex = await Record.ExceptionAsync(async () => await Act(getFulfillmentPolicies));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<EmptyMarketplaceException>();
        }
        [Fact]
        public async Task given_null_as_marketplace_should_throw_an_exception()
        {
            const string marketplace = null;
            Guid id = Guid.NewGuid();

            GetReturnPolicies getFulfillmentPolicies = new GetReturnPolicies
            {
                Marketplace = marketplace,
                UserId = id
            };

            Exception ex = await Record.ExceptionAsync(async () => await Act(getFulfillmentPolicies));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<EmptyMarketplaceException>();
        }

        [Fact]
        public async Task given_not_existing_user_should_throw_an_exception()
        {
            const string marketplace = "marketplace";
            Guid id = Guid.NewGuid();

            GetReturnPolicies getFulfillmentPolicies = new GetReturnPolicies
            {
                Marketplace = marketplace,
                UserId = id
            };
            
            Exception ex = await Record.ExceptionAsync(async () => await Act(getFulfillmentPolicies));
            
            ex.ShouldNotBeNull();
            ex.ShouldBeOfType<UserNotFoundException>();
        }
        
        [Fact]
        public async Task given_existing_user_should_throw_an_exception()
        {
            const string marketplace = "marketplace";
            Guid id = Guid.NewGuid();

            GetReturnPolicies getFulfillmentPolicies = new GetReturnPolicies
            {
                Marketplace = marketplace,
                UserId = id
            };

            User user = new User(id,"name","login","password","authCode","accessToken","refreshToken","marketplace",null,false);
           
            _userRepository.GetUserById(id).Returns(user);

            await Act(getFulfillmentPolicies);

            await _ebayPoliciesServiceClient.Received(1).GetReturnPolicies(marketplace, user.AccessToken,user.IsTestUser);
        }
    }
}