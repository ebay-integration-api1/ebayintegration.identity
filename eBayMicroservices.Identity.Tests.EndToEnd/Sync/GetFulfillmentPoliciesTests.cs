﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Api;
using eBayMicroservices.Identity.Infrastructure.Mongo.Documents;
using eBayMicroservices.Identity.Tests.Shared.Factories;
using eBayMicroservices.Identity.Tests.Shared.Fixture;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.EndToEnd.Sync
{
    [Collection("E2E")]
    public class GetFulfillmentPoliciesTests : IDisposable,IClassFixture<EBayMicroserviceAppFactory<Program>>
    {
        private Task<HttpResponseMessage> Act(Guid id,string marketPlace)
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(id.ToString());
            return _httpClient.GetAsync($"api/policies/fulfillment?marketplace={marketPlace}");
        }


        private readonly HttpClient _httpClient;
        private readonly MongoDbFixture<UserDocument, Guid> _mongoDbFixture;

        public GetFulfillmentPoliciesTests(EBayMicroserviceAppFactory<Program> factory)
        {
            factory.Server.AllowSynchronousIO = true;
            _httpClient = factory.CreateClient();
            _mongoDbFixture = new MongoDbFixture<UserDocument, Guid>("Users");
        }
        
        [Fact]
        public async Task get_invalid_marketplace_should_return_404()
        {
            Guid id = Guid.NewGuid();
            const string marketPlace = "";
            HttpResponseMessage response = await Act(id,marketPlace);
            
            response.ShouldNotBeNull();

            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
        }
        
        [Fact]
        public async Task get_valid_marketplace_should_return_Ok()
        {
            Guid id = Guid.NewGuid();
            await InsertResource(id, true);
            const string marketPlace = "EBAY_PL";
            HttpResponseMessage response = await Act(id,marketPlace);
            
            response.ShouldNotBeNull();

            response.StatusCode.ShouldBe(HttpStatusCode.OK);
        }
        private async Task InsertResource(Guid id,bool isTestUser)
        {
            UserDocument userDocument = new UserDocument
            {
                Id = id,
                Login = "login_from_getResourceTest",
                Password = "password_from_getResourceTest",
                Name = "name_from_getResourceTest",
                AccessToken = "access_token",
                AuthenticationCode = "auth_code",
                RefreshToken = "refresh_token",
                Marketplace = "EBAY_PL",
                IsTestUser = isTestUser
            };
            await _mongoDbFixture.InsertAsync(userDocument);
        }
        public void Dispose()
        {
            _mongoDbFixture.Dispose();
        }
    }
}