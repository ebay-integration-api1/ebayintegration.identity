﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Api;
using eBayMicroservices.Identity.Api.Models;
using eBayMicroservices.Identity.Infrastructure.Mongo.Documents;
using eBayMicroservices.Identity.Tests.Shared.Factories;
using eBayMicroservices.Identity.Tests.Shared.Fixture;
using Newtonsoft.Json;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.EndToEnd.Sync
{
    [Collection("E2E")]
    public class SignInTests : IDisposable, IClassFixture<EBayMicroserviceAppFactory<Program>>
    {

        public async Task<HttpResponseMessage> Act(LoginUserRequestModel requestModel) =>
            await _httpClient.PostAsync("api/identity/sign-in", GetContent(requestModel));

        [Fact]
        public async Task sign_in_existing_user_should_be_succeed()
        {
            Guid id = Guid.NewGuid();
            const string login = "existing_user";
            const string password = "existing_user_password";
            UserDocument userDocument = new UserDocument
            {
                Id = id,
                Login = login,
                Password = password,
                Name = "inserted_name",
            };
            await _mongoDbFixture.InsertAsync(userDocument);

            LoginUserRequestModel loginUserRequestModel = new LoginUserRequestModel
            {
                Login = login,
                Password = password
            };

            HttpResponseMessage result = await Act(loginUserRequestModel);
            
            result.ShouldNotBeNull();
            result.StatusCode.ShouldBe(HttpStatusCode.OK);
        }
        
        [Fact]
        public async Task sign_in_not_existing_user_should_be_bad_request()
        {
            const string login = "not_existing_user";
            const string password = "not_existing_user_password";
            
            LoginUserRequestModel loginUserRequestModel = new LoginUserRequestModel
            {
                Login = login,
                Password = password
            };

            HttpResponseMessage result = await Act(loginUserRequestModel);
            
            result.ShouldNotBeNull();
            result.StatusCode.ShouldBe(HttpStatusCode.NotFound);
        }
        
        
        [Fact]
        public async Task sign_in_existing_user_with_empty_password_should_be_bad_request()
        {
            Guid id = Guid.NewGuid();
            const string login = "existing_user";
            const string password = "";
            UserDocument userDocument = new UserDocument
            {
                Id = id,
                Login = login,
                Password = password,
                Name = "inserted_name",
            };
            await _mongoDbFixture.InsertAsync(userDocument);

            LoginUserRequestModel loginUserRequestModel = new LoginUserRequestModel
            {
                Login = login,
                Password = password
            };

            HttpResponseMessage result = await Act(loginUserRequestModel);
            
            result.ShouldNotBeNull();
            result.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
        }
        
        [Fact]
        public async Task sign_in_existing_user_with_password_as_null_should_be_bad_request()
        {
            Guid id = Guid.NewGuid();
            const string login = "existing_user";
            const string password = null;
            UserDocument userDocument = new UserDocument
            {
                Id = id,
                Login = login,
                Password = password,
                Name = "inserted_name",
            };
            await _mongoDbFixture.InsertAsync(userDocument);

            LoginUserRequestModel loginUserRequestModel = new LoginUserRequestModel
            {
                Login = login,
                Password = password
            };

            HttpResponseMessage result = await Act(loginUserRequestModel);
            
            result.ShouldNotBeNull();
            result.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
        }
        [Fact]
        public async Task sign_in_existing_user_with_empty_login_should_be_bad_request()
        {
            Guid id = Guid.NewGuid();
            const string login = "existing_user";
            const string password = "existing_password";
            UserDocument userDocument = new UserDocument
            {
                Id = id,
                Login = login,
                Password = password,
                Name = "inserted_name",
            };
            await _mongoDbFixture.InsertAsync(userDocument);

            LoginUserRequestModel loginUserRequestModel = new LoginUserRequestModel
            {
                Login = "",
                Password = password
            };

            HttpResponseMessage result = await Act(loginUserRequestModel);
            
            result.ShouldNotBeNull();
            result.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
        }
        
        [Fact]
        public async Task sign_in_existing_user_with_login_as_null_should_be_bad_request()
        {
            Guid id = Guid.NewGuid();
            const string login = "existing_user";
            const string password = "existing_password";
            UserDocument userDocument = new UserDocument
            {
                Id = id,
                Login = login,
                Password = password,
                Name = "inserted_name",
                
            };
            await _mongoDbFixture.InsertAsync(userDocument);

            LoginUserRequestModel loginUserRequestModel = new LoginUserRequestModel
            {
                Login = null,
                Password = password
            };

            HttpResponseMessage result = await Act(loginUserRequestModel);
            
            result.ShouldNotBeNull();
            result.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
        }
        
        #region Arrange
        
        private readonly HttpClient _httpClient;
        private readonly MongoDbFixture<UserDocument,Guid> _mongoDbFixture;

        private static StringContent GetContent(object value) =>
            new StringContent(JsonConvert.SerializeObject(value), Encoding.UTF8, "application/json");


        public SignInTests(EBayMicroserviceAppFactory<Program> factory)
        {
            _mongoDbFixture = new MongoDbFixture<UserDocument, Guid>("Users");
            factory.Server.AllowSynchronousIO = true;
            _httpClient = factory.CreateClient();
        }

        #endregion

        public void Dispose()
        {
            _mongoDbFixture.Dispose();
        }
        
    }
}