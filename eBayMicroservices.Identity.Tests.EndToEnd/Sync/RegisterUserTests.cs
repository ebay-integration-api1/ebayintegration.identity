﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Api;
using eBayMicroservices.Identity.Api.Models;
using eBayMicroservices.Identity.Infrastructure.Mongo.Documents;
using eBayMicroservices.Identity.Tests.Shared.Factories;
using eBayMicroservices.Identity.Tests.Shared.Fixture;
using Newtonsoft.Json;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.EndToEnd.Sync
{
    [Collection("E2E")]
    public class RegisterUserTests : IDisposable, IClassFixture<EBayMicroserviceAppFactory<Program>>
    {
        
        private Task<HttpResponseMessage> Act(RegisterUserRequestModel request) => _httpClient.PostAsync("api/identity/sign-up",GetContent(request));

        [Fact]
        public async Task register_user_with_existing_login_should_throw_exception()
        {
            Guid id = Guid.NewGuid();
            await InsertResource(id);
        
            RegisterUserRequestModel model = await PrepareModelAndAddMarketplace();
            
            model.Login = "inserted_login";
            HttpResponseMessage response = await Act(model);
            
            
            response.ShouldNotBeNull();
            response.StatusCode.ShouldBe(HttpStatusCode.Conflict);
        }
        
        [Fact]
        public async Task register_production_user_should_return_created()
        {
            RegisterUserRequestModel model = await PrepareModelAndAddMarketplace();

            model.Login += "aaa";
            
            HttpResponseMessage response = await Act(model);
            
            response.ShouldNotBeNull();
            response.StatusCode.ShouldBe(HttpStatusCode.Created);

            string id = response.Headers.Location.ToString().Split("/").Last();
            UserDocument inserted = await _mongoDbFixture.GetAsync(Guid.Parse(id));
            inserted.ShouldNotBeNull();
            inserted.IsTestUser.ShouldBe(false);
        }
        
        [Fact]
        public async Task register_test_user_should_create_user_in_database()
        {
            RegisterUserRequestModel model = await PrepareModelAndAddMarketplace(true);
            
            HttpResponseMessage response = await Act(model);
            
            response.ShouldNotBeNull();
            response.StatusCode.ShouldBe(HttpStatusCode.Created);

            string id = response.Headers.Location.ToString().Split("/").Last();
            UserDocument inserted = await _mongoDbFixture.GetAsync(Guid.Parse(id));
            inserted.ShouldNotBeNull();
            inserted.IsTestUser.ShouldBe(true);

        }
        
        [Fact]
        public async Task register_user_should_create_user_in_database()
        {
            var model = await PrepareModelAndAddMarketplace();
            
            HttpResponseMessage response = await Act(model);
            
            response.ShouldNotBeNull();
            response.StatusCode.ShouldBe(HttpStatusCode.Created);

            Uri p = response.Headers.Location;

            string id = p.ToString().Split("/").Last();

            id.ShouldNotBeNull();
            
            UserDocument doc = await _mongoDbFixture.GetAsync(Guid.Parse(id));
            
            doc.ShouldNotBeNull();
            doc.Login.ShouldBe(model.Login);

        }
        
        [Fact]
        public async Task register_without_login()
        {
            RegisterUserRequestModel registerUserRequestModel = new RegisterUserRequestModel
            {
                Name = "name",
                Password = "password",
                AuthenticationCode = "auth_code",
            };

            HttpResponseMessage response = await Act(registerUserRequestModel);
            
            response.ShouldNotBeNull();
            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
        }
        [Fact]
        public async Task register_without_name()
        {
            RegisterUserRequestModel registerUserRequestModel = new RegisterUserRequestModel
            {
                Login = "login",
                Password = "password",
                AuthenticationCode = "auth_code",
            };

            HttpResponseMessage response = await Act(registerUserRequestModel);
            
            response.ShouldNotBeNull();
            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
        }
        [Fact]
        public async Task register_without_password()
        {
            RegisterUserRequestModel registerUserRequestModel = new RegisterUserRequestModel
            {
                Login = "login",
                Name = "name",
                AuthenticationCode = "auth_code",
            };

            HttpResponseMessage response = await Act(registerUserRequestModel);
            
            response.ShouldNotBeNull();
            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
        }
        
        [Fact]
        public async Task register_without_authCode()
        {
            RegisterUserRequestModel registerUserRequestModel = new RegisterUserRequestModel
            {
                Login = "login",
                Name = "name",
                Password = "password"
            };

            HttpResponseMessage response = await Act(registerUserRequestModel);
            
            response.ShouldNotBeNull();
            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
        }
        
        [Fact]
        public async Task register_with_invalid_marketplace()
        {
            RegisterUserRequestModel registerUserRequestModel = new RegisterUserRequestModel
            {
                Login = "login",
                Password = "password",
                AuthenticationCode = "auth_code",
                Marketplace = "not_existing_code"
            };

            HttpResponseMessage response = await Act(registerUserRequestModel);
            
            response.ShouldNotBeNull();
            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
        }
        
        #region Arrange
        
        private async Task<RegisterUserRequestModel> PrepareModelAndAddMarketplace(bool testUser=false)
        {
            await _marketplaceFixture.InsertAsync(new MarketplaceDocument {Code = "EBAY_PL", Name = "PL"});
            
            return new RegisterUserRequestModel
            {
                Login = $"login{DateTime.Now}",
                Name = "name",
                Password = "password",
                AuthenticationCode = "authCode",
                Marketplace = "EBAY_PL",
                IsTestUser = testUser
            };
        }

        private async Task InsertResource(Guid id)
        {
            UserDocument userDocument = new UserDocument
            {
                Id = id,
                Login = "inserted_login",
                Password = "inserted_password",
                Name = "inserted_name",
                AuthenticationCode = "authCode"
            };
            await _mongoDbFixture.InsertAsync(userDocument);
        }
        
        private readonly HttpClient _httpClient;
        private readonly MongoDbFixture<UserDocument,Guid> _mongoDbFixture;
        private readonly MongoDbFixture<MarketplaceDocument,string> _marketplaceFixture;

        private static StringContent GetContent(object value) =>
            new StringContent(JsonConvert.SerializeObject(value), Encoding.UTF8, "application/json");


        public RegisterUserTests(EBayMicroserviceAppFactory<Program> factory)
        {
            _mongoDbFixture = new MongoDbFixture<UserDocument, Guid>("Users");
            _marketplaceFixture = new MongoDbFixture<MarketplaceDocument, string>("Marketplaces");
            
            factory.Server.AllowSynchronousIO = true;
            _httpClient = factory.CreateClient();
        }

        #endregion

        public void Dispose()
        {
            _mongoDbFixture.Dispose();
        }
    }
}