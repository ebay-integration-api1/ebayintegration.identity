﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Api;
using eBayMicroservices.Identity.Infrastructure.Mongo.Documents;
using eBayMicroservices.Identity.Tests.Shared.Factories;
using eBayMicroservices.Identity.Tests.Shared.Fixture;
using Newtonsoft.Json;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.EndToEnd.Sync
{
    [Collection("E2E")]
    public class GetUserTests : IDisposable,IClassFixture<EBayMicroserviceAppFactory<Program>>
    {
        private Task<HttpResponseMessage> Act(Guid id) => _httpClient.GetAsync($"api/identity/{id}");

        [Fact]
        public async Task get_existing_production_user_should_return_dto()
        {
            Guid id = Guid.NewGuid();
            const bool isTestUser = false;
            await InsertResource(id,isTestUser);
            
            HttpResponseMessage response = await Act(id);
            
            response.ShouldNotBeNull();
        
            response.StatusCode.ShouldBe(HttpStatusCode.OK);
            
            string cont = await response.Content.ReadAsStringAsync();
            
            UserDocument userDto = JsonConvert.DeserializeObject<UserDocument>(cont);
            
            userDto.Id.ShouldBe(id);
            userDto.IsTestUser.ShouldBe(isTestUser);
        }
        [Fact]
        public async Task get_existing_test_user_should_return_dto()
        {
            Guid id = Guid.NewGuid();
            const bool isTestUser = true;
            await InsertResource(id,isTestUser);
            
            HttpResponseMessage response = await Act(id);
            
            response.ShouldNotBeNull();
        
            response.StatusCode.ShouldBe(HttpStatusCode.OK);
            
            string cont = await response.Content.ReadAsStringAsync();
            
            UserDocument userDto = JsonConvert.DeserializeObject<UserDocument>(cont);
            
            userDto.Id.ShouldBe(id);
            userDto.IsTestUser.ShouldBe(isTestUser);
        }
        [Fact]
        public async Task get_not_existing_user_should_return_404()
        {
            Guid id = Guid.NewGuid();
            HttpResponseMessage response = await Act(id);
            
            response.ShouldNotBeNull();
            response.StatusCode.ShouldBe(HttpStatusCode.NotFound);
         }
        
        #region Arrange

        private readonly HttpClient _httpClient;
        private readonly MongoDbFixture<UserDocument,Guid> _mongoDbFixture;
        

        private async Task InsertResource(Guid id,bool isTestUser)
        {
            UserDocument userDocument = new UserDocument
            {
                Id = id,
                Login = "login_from_getResourceTest",
                Password = "password_from_getResourceTest",
                Name = "name_from_getResourceTest",
                AccessToken = "access_token",
                AuthenticationCode = "auth_code",
                RefreshToken = "refresh_token",
                Marketplace = "EBAY_PL",
                IsTestUser = isTestUser
            };
            await _mongoDbFixture.InsertAsync(userDocument);
        }

        public GetUserTests(EBayMicroserviceAppFactory<Program> factory)
        {
            _mongoDbFixture = new MongoDbFixture<UserDocument, Guid>("Users");
            factory.Server.AllowSynchronousIO = true;
            _httpClient = factory.CreateClient();
            
        }

        #endregion

        public void Dispose()
        {
            _mongoDbFixture.Dispose();
        }
    }
}