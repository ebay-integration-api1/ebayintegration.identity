﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Api;
using eBayMicroservices.Identity.Api.Models;
using eBayMicroservices.Identity.Tests.Shared.Factories;
using Newtonsoft.Json;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.EndToEnd.Sync
{
    [Collection("E2E")]
    public class GetEBayRedirectUrlToSignIn : IDisposable, IClassFixture<EBayMicroserviceAppFactory<Program>>
    {
        private Task<HttpResponseMessage> Act(bool? sandbox) => _httpClient.GetAsync(!sandbox.HasValue ? "api/identity/getUrlToSignIn" : $"api/identity/getUrlToSignIn?sandbox={sandbox.Value}");

        [Fact]
        public async Task get_url_with_sandbox_flag_as_null_should_return_bad_request()
        {
            HttpResponseMessage response = await Act(null);

            response.ShouldNotBeNull();

            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
        }
        
        [Fact]
        public async Task get_url_with_sandbox_flag_as_false_should_return_link_without_sandbox()
        {
            HttpResponseMessage response = await Act(false);

            response.ShouldNotBeNull();

            response.StatusCode.ShouldBe(HttpStatusCode.OK);

            string result = await response.Content.ReadAsStringAsync();

            GetUrlToSignInResponse responseModel = JsonConvert.DeserializeObject<GetUrlToSignInResponse>(result);
            
            responseModel.RedirectUrl.ShouldNotBeNull();
            responseModel.RedirectUrl.ShouldNotContain("sandbox");
            
        }
        
        [Fact]
        public async Task get_url_with_sandbox_flag_as_true_should_return_link_with_sandbox()
        {
            HttpResponseMessage response = await Act(true);

            response.ShouldNotBeNull();

            response.StatusCode.ShouldBe(HttpStatusCode.OK);
            
            string result = await response.Content.ReadAsStringAsync();

            GetUrlToSignInResponse responseModel = JsonConvert.DeserializeObject<GetUrlToSignInResponse>(result);
            
            responseModel.RedirectUrl.ShouldNotBeNull();
            responseModel.RedirectUrl.ShouldContain("sandbox");
        }


        #region Arrange

        private readonly HttpClient _httpClient;

        public GetEBayRedirectUrlToSignIn(EBayMicroserviceAppFactory<Program> factory)
        {
            factory.Server.AllowSynchronousIO = true;
            _httpClient = factory.CreateClient();
        }

        #endregion

        public void Dispose()
        {
        }
    }
}