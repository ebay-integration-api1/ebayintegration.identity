﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Api;
using eBayMicroservices.Identity.Infrastructure.Mongo.Documents;
using eBayMicroservices.Identity.Tests.Shared.Factories;
using eBayMicroservices.Identity.Tests.Shared.Fixture;
using Newtonsoft.Json;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.EndToEnd.Sync
{
    [Collection("E2E")]
    public class TriggerAuthCodeRefreshTests : IDisposable,IClassFixture<EBayMicroserviceAppFactory<Program>>
    {
        private Task<HttpResponseMessage> Act() 
            => _httpClient.PutAsync($"api/identity/tokens",
                new StringContent(JsonConvert.SerializeObject(new {}),Encoding.UTF8, "application/json"));
        
            #region Arrange

            private readonly HttpClient _httpClient;
            private readonly MongoDbFixture<UserDocument,Guid> _mongoDbFixture;

            [Fact]
            public async Task test_refresh_tokens_on_existing_users()
            {
                Guid idWithNoTokens = Guid.NewGuid(); 
                Guid idWithTokensToReloadFarFromTime = Guid.NewGuid(); 
                Guid idWithTokensToReloadCloserTime = Guid.NewGuid(); 
                Guid idWithTokensToReloadMoreCloserTime = Guid.NewGuid(); 
                Guid idWithTokensToReloadNearTime = Guid.NewGuid();

                long ticksBefore50 = DateTime.UtcNow.AddMinutes(-50).Ticks;
                long ticksBefore25 = DateTime.UtcNow.AddMinutes(-25).Ticks;
                long ticksBefore18 = DateTime.UtcNow.AddMinutes(-18).Ticks;
                long ticksBefore5 = DateTime.UtcNow.AddMinutes(-5).Ticks;

                string refreshTokenInitial = "refreshTokenInitial";

                IList<UserDocument> users = new List<UserDocument>
                {
                    new UserDocument
                    {
                        Id = idWithNoTokens,
                        Login = "login_from_getResourceTest",
                        Password = "password_from_getResourceTest",
                        Name = "name_from_getResourceTest",
                        AccessToken = null,
                        AuthenticationCode = "auth_code",
                        RefreshToken = null
                    },
                    new UserDocument
                    {
                        Id = idWithTokensToReloadFarFromTime,
                        Login = "login_from_getResourceTest",
                        Password = "password_from_getResourceTest",
                        Name = "name_from_getResourceTest",
                        AccessToken = "example",
                        AuthenticationCode = "auth_code",
                        RefreshToken = refreshTokenInitial,
                        SetTokenDateTime = ticksBefore50
                    },
                    new UserDocument
                    {
                        Id = idWithTokensToReloadCloserTime,
                        Login = "login_from_getResourceTest",
                        Password = "password_from_getResourceTest",
                        Name = "name_from_getResourceTest",
                        AccessToken = "example",
                        AuthenticationCode = "auth_code",
                        RefreshToken = refreshTokenInitial,
                        SetTokenDateTime = ticksBefore25
                    },
                    new UserDocument
                    {
                        Id = idWithTokensToReloadMoreCloserTime,
                        Login = "login_from_getResourceTest",
                        Password = "password_from_getResourceTest",
                        Name = "name_from_getResourceTest",
                        AccessToken = "example",
                        AuthenticationCode = "auth_code",
                        RefreshToken = refreshTokenInitial,
                        SetTokenDateTime = ticksBefore18
                    },
                    new UserDocument
                    {
                        Id = idWithTokensToReloadNearTime,
                        Login = "login_from_getResourceTest",
                        Password = "password_from_getResourceTest",
                        Name = "name_from_getResourceTest",
                        AccessToken = "example",
                        AuthenticationCode = "auth_code",
                        RefreshToken = refreshTokenInitial,
                        SetTokenDateTime = ticksBefore5
                    }
                };
                foreach (UserDocument userDocument in users)
                {
                    await _mongoDbFixture.InsertAsync(userDocument);
                }

                HttpResponseMessage z = await Act();

                z.ShouldNotBeNull();
                z.StatusCode.ShouldBe(HttpStatusCode.NoContent);

                UserDocument userWithoutCodes = await _mongoDbFixture.GetAsync(idWithNoTokens);
                
                userWithoutCodes.SetTokenDateTime.ShouldNotBeNull();
                userWithoutCodes.AccessToken.ShouldNotBeNull();
                userWithoutCodes.RefreshToken.ShouldNotBeNull();

                
                UserDocument userWithTimeMinus50 = await _mongoDbFixture.GetAsync(idWithTokensToReloadFarFromTime);
                
                userWithTimeMinus50.SetTokenDateTime.ShouldNotBeNull();
                userWithTimeMinus50.AccessToken.ShouldNotBeNull();
                userWithTimeMinus50.RefreshToken.ShouldNotBeNull();
                userWithTimeMinus50.SetTokenDateTime.ShouldNotBe(ticksBefore50);

                UserDocument userWithTimeMinus25 = await _mongoDbFixture.GetAsync(idWithTokensToReloadCloserTime);
                
                userWithTimeMinus25.SetTokenDateTime.ShouldNotBeNull();
                userWithTimeMinus25.AccessToken.ShouldNotBeNull();
                userWithTimeMinus25.RefreshToken.ShouldNotBeNull();
                userWithTimeMinus25.SetTokenDateTime.ShouldNotBe(ticksBefore25);
                
                UserDocument userWithTimeMinus18 = await _mongoDbFixture.GetAsync(idWithTokensToReloadMoreCloserTime);
                
                userWithTimeMinus18.SetTokenDateTime.ShouldNotBeNull();
                userWithTimeMinus18.AccessToken.ShouldNotBeNull();
                userWithTimeMinus18.RefreshToken.ShouldNotBeNull();
                userWithTimeMinus18.SetTokenDateTime.ShouldBe(ticksBefore18);

                UserDocument userWithTimeMinus5 = await _mongoDbFixture.GetAsync(idWithTokensToReloadNearTime);
                
                userWithTimeMinus5.SetTokenDateTime.ShouldNotBeNull();
                userWithTimeMinus5.AccessToken.ShouldNotBeNull();
                userWithTimeMinus5.RefreshToken.ShouldNotBeNull();
                userWithTimeMinus5.SetTokenDateTime.ShouldBe(ticksBefore5);

            }

            public TriggerAuthCodeRefreshTests(EBayMicroserviceAppFactory<Program> factory)
            {
                _mongoDbFixture = new MongoDbFixture<UserDocument, Guid>("Users");
                factory.Server.AllowSynchronousIO = true;
                _httpClient = factory.CreateClient();
            
            }

            #endregion

            public void Dispose()
            {
                _mongoDbFixture.Dispose();
            }
    }
}