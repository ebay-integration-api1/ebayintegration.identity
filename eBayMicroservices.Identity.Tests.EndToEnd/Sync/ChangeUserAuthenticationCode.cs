﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Api;
using eBayMicroservices.Identity.Api.Models;
using eBayMicroservices.Identity.Infrastructure.Mongo.Documents;
using eBayMicroservices.Identity.Tests.Shared.Factories;
using eBayMicroservices.Identity.Tests.Shared.Fixture;
using Newtonsoft.Json;
using Shouldly;
using Xunit;

namespace eBayMicroservices.Identity.Tests.EndToEnd.Sync
{
    [Collection("E2E")]
    public class ChangeUserAuthenticationCode : IDisposable,IClassFixture<EBayMicroserviceAppFactory<Program>>
    {
        
    
        private Task<HttpResponseMessage> Act(Guid? id,string code)
        {
            if (id.HasValue)
            {
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(id.ToString());
            }
            return _httpClient.PutAsync($"api/identity/auth-code",
                new StringContent(JsonConvert.SerializeObject(new UpdateAuthCodeRequestModel
                {
                    Code = code
                }), Encoding.UTF8, "application/json"));
        }


        [Fact]
        public async Task empty_code_to_update_should_return_400()
        {
            Guid id = Guid.NewGuid();
            await InsertResource(id);
            HttpResponseMessage response = await Act(id,null);
            
            response.ShouldNotBeNull();
        
            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
        }
        
        [Fact]
        public async Task invalid_code_to_update_should_return_400()
        {
            Guid id = Guid.NewGuid();
            await InsertResource(id);
            string code = "";
            HttpResponseMessage response = await Act(id,code);
            
            response.ShouldNotBeNull();
        
            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
        }
        
        [Fact]
        public async Task get_existing_user_should_return_dto()
        {
            Guid id = Guid.NewGuid();
            await InsertResource(id);
            string code = "code";
            HttpResponseMessage response = await Act(id,code);
            
            response.ShouldNotBeNull();
        
            response.StatusCode.ShouldBe(HttpStatusCode.NoContent);
            
            UserDocument z = await WaitForCompleteAccessAndRefreshTokens(id);

            z.ShouldNotBeNull();
            z.AuthenticationCode.ShouldBe(code);
            z.RefreshToken.ShouldNotBeNull();
            z.AccessToken.ShouldNotBeNull();
        }

        private async Task<UserDocument> WaitForCompleteAccessAndRefreshTokens(Guid id)
        {
            UserDocument z=null;
            for (int i = 0; i < 4; i++)
            {
                z = await _mongoDbFixture.GetAsync(id);
                if (z.RefreshToken is null)
                {
                    await Task.Delay(2000);
                    continue;
                }

                break;
            }

            return z;
        }

        [Fact]
        public async Task update_for_not_existing_user_should_return_404()
        {
            Guid id = Guid.NewGuid();
            string code = "newCode";
            HttpResponseMessage response = await Act(id,code);
            
            response.ShouldNotBeNull();
            response.StatusCode.ShouldBe(HttpStatusCode.NotFound);
         }
        
        #region Arrange

        private readonly HttpClient _httpClient;
        private readonly MongoDbFixture<UserDocument,Guid> _mongoDbFixture;
        

        private async Task InsertResource(Guid id)
        {
            UserDocument userDocument = new UserDocument
            {
                Id = id,
                Login = "login_from_getResourceTest",
                Password = "password_from_getResourceTest",
                Name = "name_from_getResourceTest",
                AccessToken = "access_token",
                AuthenticationCode = "auth_code",
                RefreshToken = "refresh_token"
            };
            await _mongoDbFixture.InsertAsync(userDocument);
        }

        public ChangeUserAuthenticationCode(EBayMicroserviceAppFactory<Program> factory)
        {
            _mongoDbFixture = new MongoDbFixture<UserDocument, Guid>("Users");
            factory.Server.AllowSynchronousIO = true;
            _httpClient = factory.CreateClient();
            
        }

        #endregion

        public void Dispose()
        {
            _mongoDbFixture.Dispose();
        }
    }
}