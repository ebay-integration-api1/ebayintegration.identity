﻿using System;
using Microsoft.Extensions.Configuration;

namespace eBayMicroservices.Identity.Tests.Shared.Helpers
{
    public class OptionsHelper
    {
        public static TSettings GetOptions<TSettings>(string section, string settingsFileName = null) where TSettings : class, new()
        {
            
            
            var p = Environment.GetEnvironmentVariable("RUNNER");
            settingsFileName ??= (p != "linux")?  "appsettings.tests.json" : "appsettings.tests_in_runner.json";
            Console.WriteLine($"RUNNER variable is : {p}, {settingsFileName}");
            var configuration = new TSettings();
            
            GetConfigurationRoot(settingsFileName)
                .GetSection(section)
                .Bind(configuration);

            return configuration;
        }
        
        private static IConfigurationRoot GetConfigurationRoot(string settingsFileName)
            => new ConfigurationBuilder()
                .AddJsonFile(settingsFileName, optional: true)
                .AddEnvironmentVariables()
                .Build();
    }
}