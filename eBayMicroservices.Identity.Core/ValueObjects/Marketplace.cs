﻿namespace eBayMicroservices.Identity.Core.ValueObjects
{
    public class Marketplace
    {
        public string Code { get; }
        public string Name { get; }

        public Marketplace(string code, string name)
        {
            Code = code;
            Name = name;
        }
    }
}