﻿using System;
using eBayMicroservices.Identity.Core.Events.Concrete;
using eBayMicroservices.Identity.Core.Exceptions;

// ReSharper disable ParameterOnlyUsedForPreconditionCheck.Local

namespace eBayMicroservices.Identity.Core.Entities
{
    public class User : AggregateRoot
    {
        public string Name { get; private set; }
        public string AuthenticationCode { get; private set; }
        public string AccessToken { get; private set; }
        public string RefreshToken { get; private set; }
        public long? SetTokenDateTime { get; private set; }
        public string Login { get; private set; }
        public string Password { get; private set; }
        public string Marketplace { get; private set; }
        public bool IsTestUser { get; }

        public User(Guid id,string name, string login, string password, string authenticationCode,string accessToken,string refreshToken, string marketplace,long? setTokenDateTime,bool isTestUser)
        {
            Id = new AggregateId(id);
            Name = name;
            Login = login;
            Password = password;
            Marketplace = marketplace;
            AuthenticationCode = authenticationCode;
            AccessToken = accessToken;
            RefreshToken = refreshToken;
            SetTokenDateTime = setTokenDateTime;
            IsTestUser = isTestUser;
        }
        
        public static User Create(Guid id,string name, string login, string password,string authenticationCode, string marketplace,bool isTestUser)
        {
            Validate(name, login, password, authenticationCode);
            
            User user = new User(id,name,login,password,authenticationCode,null,null,marketplace,null,isTestUser);
            
            user.AddEvent(new UserCreated(user));
            
            return user;
        }

        public void UpdateAuthenticationCode(string authenticationCode)
        {
            if (string.IsNullOrWhiteSpace(authenticationCode))
            {
                throw new InvalidUserDataException("authenticationCode");
            }

            AuthenticationCode = authenticationCode;
            
            AddEvent(new AuthenticationCodeUpdated(this));

        }

        private static void Validate(string name, string login, string password, string authenticationCode)
        {
            if (string.IsNullOrWhiteSpace(login))
            {
                throw new InvalidUserDataException(nameof(login));
            }

            if (string.IsNullOrWhiteSpace(name))
            {
                throw new InvalidUserDataException(nameof(name));
            }

            if (string.IsNullOrWhiteSpace(password))
            {
                throw new InvalidUserDataException(nameof(password));
            }

            if (string.IsNullOrWhiteSpace(authenticationCode))
            {
                throw new InvalidUserDataException(nameof(authenticationCode));
            }
        }
    }
}