﻿using eBayMicroservices.Identity.Core.Entities;
using eBayMicroservices.Identity.Core.Events.Abstract;

namespace eBayMicroservices.Identity.Core.Events.Concrete
{
    public class AuthenticationCodeUpdated : IDomainEvent
    {
        public User User { get; }

        public AuthenticationCodeUpdated(User user)
        {
            User = user;
        }
    }
}