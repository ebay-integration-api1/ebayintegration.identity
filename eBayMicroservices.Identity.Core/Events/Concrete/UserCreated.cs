﻿using eBayMicroservices.Identity.Core.Entities;
using eBayMicroservices.Identity.Core.Events.Abstract;

namespace eBayMicroservices.Identity.Core.Events.Concrete
{
    public class UserCreated : IDomainEvent
    {
        public User User { get; }

        public UserCreated(User user)
        {
            User = user;
        }
    }
}