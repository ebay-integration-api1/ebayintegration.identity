﻿using eBayMicroservices.Identity.Core.Exceptions.Abstract;

namespace eBayMicroservices.Identity.Core.Exceptions
{
    public class InvalidUserDataException : DomainException
    {
        public string Field { get; }

        public InvalidUserDataException(string field) : base($"invalid user {field}")
        {
            Field = field;
        }

        public override string Code => $"invalid_user_{Field}";
    }
}