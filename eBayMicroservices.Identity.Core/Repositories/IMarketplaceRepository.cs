﻿using System.Threading.Tasks;

namespace eBayMicroservices.Identity.Core.Repositories
{
    public interface IMarketplaceRepository
    {
        Task<bool> ExistsAsync(string code);
    }
}