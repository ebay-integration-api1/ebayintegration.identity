﻿using System;
using System.Threading.Tasks;
using eBayMicroservices.Identity.Core.Entities;

namespace eBayMicroservices.Identity.Core.Repositories
{
    public interface IUserRepository
    {
        Task AddAsync(User user);
        Task UpdateUserTokens(Guid id,string accessToken,string refreshToken,string expiresIn);
        Task UpdateUserTokens(Guid id, string accessToken, string expiresIn);
        Task UpdateUserTokensAsInvalid(Guid id);
        Task UpdateUserAuthenticationCode(Guid id,string authenticationCode);
        Task<bool> ExistsAsync(string login);
        Task<User> GetUserToRefreshToken();
        Task<User> GetUserByLogin(string login);
        Task<User> GetUserById(Guid id);
    }
}